/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mo.eoc.wrapper;

import org.featureSelection.cibm.CMFeatureSelection;
import org.featureSelection.weka.FeatureSelection;
import static org.functionlib.ensembleFunctions.getFSName;
import static org.functionlib.wekaFunctions.loadArff;
import static org.functionlib.wekaFunctions.saveArff;
import weka.core.Instances;
import weka.core.Utils;

/**
 *
 * @author c3172331
 */
public class MainFeatureSelection {

    public static FeatureSelection wekaFS;
    public static CMFeatureSelection cmFS;
    public static int featureSelNo;
    public static String relName;
    private static String strPath = "", tstFile = "", resFile = "";
    static int numberOfSelFeatures;
    static Instances trnData = null;
    static Instances tstData = null;
    public static int nClass = 2;
    public static boolean isMultiClass = false;

    private static String arffFile = "";

    static void initFeatureRanks(Instances trnData) {
        wekaFS = new FeatureSelection(trnData);
        wekaFS.generateRanks();
        cmFS = new CMFeatureSelection(trnData);
        cmFS.generateRanks();
    }

    public static void readCmdOptions(String[] args) throws Exception {
        arffFile = Utils.getOption("t", args);
        System.out.println("Train File: " + arffFile);

        strPath = Utils.getOption("o", args);
        if (!strPath.isEmpty()) {
            System.out.println("Output Path: " + strPath);
        }

        String strMaxEval = Utils.getOption("f", args);
        if (!strMaxEval.isEmpty()) {
            System.out.println("Feature Selection Method: " + strMaxEval);
            featureSelNo = Integer.parseInt(strMaxEval);
        }

        String strPopSize = Utils.getOption("n", args);
        if (!strPopSize.isEmpty()) {
            System.out.println("Features to select: " + strPopSize);
            numberOfSelFeatures = Integer.parseInt(strPopSize);
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        // output usage
        if (args.length == 0) {
            System.err.println("\nUsage: java -jar MO-EoC-Wrapper.jar\n\t"
                    + "-t <training arff file>\n\t"
                    + "-o <output path>\n\t"
                    + "-f <feature selection method name 0 to 6>\n\t"
                    + "-n <no of selected fetaure>\n"
            );
            System.exit(1);
        } else {
            readCmdOptions(args);
        }

        // Load Training data        
        trnData = loadArff(arffFile);
        relName = trnData.relationName();
        int nFeatures = trnData.numAttributes() - 1;
        nClass = trnData.numClasses();

        System.out.println("Dataset:" + relName + "\tFeatures: " + nFeatures + "\t Samples:" + trnData.numInstances());

// do the Features Ranking
        /*
        CMScoreCalculator cmScr = new CMScoreCalculator(trnData, false);
        cmScr.CalculateCM1();
        cmScr.showCM1Scores();
        
        
        cmScr.CalculateCM2();
        cmScr.showCM2Scores();
        */
        
         initFeatureRanks(trnData);
        System.out.println("Init Feature Rank Done!");
        
        Instances data = null;
        if (featureSelNo > 1) {
            data = wekaFS.getArffWithTopFeatures(featureSelNo, numberOfSelFeatures);
        } else {
            data = cmFS.getArffWithTopFeatures(featureSelNo, numberOfSelFeatures);
        }

        
        String newRelName = relName + "-" + getFSName(featureSelNo) + "-Top-" + numberOfSelFeatures;
        String outFile = strPath + "/" + newRelName + ".arff";
        data.setRelationName(newRelName);
        System.out.println(outFile);
        saveArff(data, outFile);
        int newFeat = data.numAttributes() - 1;
        System.out.println("Dataset:" + newRelName + "\tFeatures: " + newFeat + "\t Samples:" + data.numInstances());

        System.exit(0);
    }
}

/// -t /home/mohammad/MO-EoC_Wraper-Parallel/InputDataset/NISP2003/arcene_train-EntF.arff -f /home/mohammad/MO-EoC_Wraper-Parallel/NIPS-MO-EoC_Wrapper-arcene.txt -T /home/mohammad/MO-EoC_Wraper-Parallel/InputDataset/NISP2003/arcene_test.arff -o /home/mohammad/MO-EoC_Wraper-Parallel/OutputDataset/NIPS2003/
// -t /home/mohammad/MO-EoC_Wraper-Parallel/InputDataset/NISP2003/arcene_train-EntF.arff -o /home/mohammad/MO-EoC_Wraper-Parallel/OutputDataset/NIPS2003/


// -t /home/mohammad/MO-EoC_Wraper-Parallel/MO-EoC-Wrapper/train-0.arff -o /home/mohammad/MO-EoC_Wraper-Parallel/MO-EoC-Wrapper/ -M true
