/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mo.eoc.wrapper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eocframework.problem.eoc.ObjMCCDiversity;
import org.eocframework.problem.eval.EvaluateMOEnsembleObjs;
import org.featureSelection.cibm.CMFeatureSelection;
import org.featureSelection.weka.FeatureSelection;
import static org.functionlib.ensembleFunctions.getFSName;
import static org.functionlib.wekaFunctions.evaluateBaseClassifiers;
import static org.functionlib.wekaFunctions.evaluateWekaEoC;
import static org.functionlib.wekaFunctions.getTopNFeatureData;
import static org.functionlib.wekaFunctions.loadArff;
import static org.functionlib.wekaFunctions.reduceDimensionality;
import static org.functionlib.wekaFunctions.saveArff;
import static org.functionlib.wekaFunctions.tenFoldBaseClassifiers;
import static org.functionlib.wekaFunctions.tenFoldWekaEoC;
import org.moeaframework.Executor;
import org.moeaframework.analysis.sensitivity.ResultEntry;
import org.moeaframework.analysis.sensitivity.ResultFileWriter;
import org.moeaframework.core.Algorithm;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Problem;
import org.moeaframework.core.Solution;
import org.moeaframework.core.spi.AlgorithmFactory;
import org.moeaframework.core.spi.ProblemFactory;
import org.moeaframework.util.TypedProperties;
import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.core.Utils;
import weka.classifiers.Classifier;

import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.MLPClassifier;
import weka.classifiers.functions.RBFNetwork;
import weka.classifiers.functions.SGD;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.SPegasos;
import weka.classifiers.functions.SimpleLogistic;
import weka.classifiers.functions.VotedPerceptron;
import weka.classifiers.lazy.IBk;
import weka.classifiers.meta.AdaBoostM1;
import weka.classifiers.meta.Bagging;
import weka.classifiers.meta.RandomCommittee;
import weka.classifiers.meta.Stacking;
import weka.classifiers.misc.VFI;
import weka.classifiers.rules.ConjunctiveRule;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.rules.JRip;
import weka.classifiers.rules.PART;
import weka.classifiers.rules.Ridor;
import weka.classifiers.trees.ADTree;
import weka.classifiers.trees.BFTree;
import weka.classifiers.trees.DecisionStump;
import weka.classifiers.trees.ExtraTree;
import weka.classifiers.trees.FT;
import weka.classifiers.trees.HoeffdingTree;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.LADTree;
import weka.classifiers.trees.LMT;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.RandomTree;
import weka.classifiers.trees.SimpleCart;

/**
 *
 * @author c3172331
 */
public class MainProgram {

    public static FeatureSelection wekaFS;
    public static CMFeatureSelection cmFS;
    public static int nFeatures;
    public static String relName;
    public static int nRuns;
    public static final int fsLen = 3; // 3 bits for FS method
    public static final int pctLen = 7; // 7 bits for percentage
    private static boolean isFilter = false;
    private static String strPath = "", tstFile = "", resFile = "";
    private static boolean isTest = false, isRes = false;
    static int numberOfSelFeatures;
    static ArrayList<String> run = new ArrayList<String>();
    static ArrayList<String> ensmCmb = new ArrayList<String>();
    static ArrayList<String> fullCMB = new ArrayList<String>();
    static ArrayList<String> fsInfo = new ArrayList<String>();
    //static ArrayList<String> mcc = new ArrayList<String>();
    //static ArrayList<String> div = new ArrayList<String>();
    static Instances trnData = null;
    static Instances tstData = null;
    public static int nClass = 2;
    public static boolean isMultiClass = false;
    public static boolean isCV = false;
    public static int kFold = 2;
    private static boolean fsLoaded = false;

    private static int popSize = 100;  //100
    private static int maxEval = 10000; //10000
    private static String arffFile = "";

    public static boolean globalDebug = false;

// Selected 29 Base Classifier
/*    private static String[] clsfNames = new String[]{
     "BayesNet", "NaiveBayes",
     "SMO", "SPegasos",
     "Logistic", "SimpleLogistic", "SGD",
     "MLPClassifier", "RBFNetwork", "VotedPerceptron",
     "ADTree", "BFTree", "HoeffdingTree", "J48", "LADTree", "REPTree", "PART",
     "DecisionStump",
     "ExtraTree", "FT", "LMT", "RandomTree", "SimpleCart",
     "IBk",
     "ConjunctiveRule", "JRip", "Ridor",
     "DecisionTable",
     "VFI"
     };

     private static String[] multiClsfNames = new String[]{
     "BayesNet", "NaiveBayes",
     "SMO",
     "Logistic", "SimpleLogistic",
     "MLPClassifier", "RBFNetwork",
     "BFTree", "HoeffdingTree", "J48", "LADTree", "REPTree", "PART",
     "DecisionStump",
     "ExtraTree", "FT", "LMT", "RandomTree", "SimpleCart",
     "IBk",
     "ConjunctiveRule", "JRip", "Ridor",
     "DecisionTable",
     "VFI"
     };
     */
    public static Classifier[] baseClassifiersMultiClass = {
        new BayesNet(), new NaiveBayes(),
        new SMO(),
        new Logistic(), new SimpleLogistic(),
        new MLPClassifier(), new RBFNetwork(),
        new BFTree(), new HoeffdingTree(), new J48(), new LADTree(), new REPTree(), new PART(),
        new DecisionStump(),
        new ExtraTree(), new FT(), new LMT(), new RandomTree(), new SimpleCart(),
        new IBk(),
        new ConjunctiveRule(), new JRip(), new Ridor(),
        new DecisionTable(),
        new VFI()};

    public static Classifier[] baseClassifiers = {
        new BayesNet(), new NaiveBayes(),
        new SMO(), new SPegasos(),
        new Logistic(), new SimpleLogistic(), new SGD(),
        new MLPClassifier(), new RBFNetwork(), new VotedPerceptron(),
        new ADTree(), new BFTree(), new HoeffdingTree(), new J48(), new LADTree(), new REPTree(), new PART(),
        new DecisionStump(),
        new ExtraTree(), new FT(), new LMT(), new RandomTree(), new SimpleCart(),
        new IBk(),
        new ConjunctiveRule(), new JRip(), new Ridor(),
        new DecisionTable(),
        new VFI()};

    public static Classifier[] wekaEoC = {
        new AdaBoostM1(), new Bagging(),
        new RandomCommittee(), new Stacking()};

    //  "SPegasos", "SGD", "VotedPerceptron", "ADTree", 
    public static Classifier[] baseClsf = null;

    static void initFeatureRanks(Instances trnData) {
        wekaFS = new FeatureSelection(trnData);
        wekaFS.generateRanks();
        cmFS = new CMFeatureSelection(trnData);
        cmFS.generateRanks();
    }

    static String longToStringTime(long totTime) {
        String timeStr = "";
        long seconds = (long) (totTime / 1000) % 60;
        long minutes = (long) ((totTime / (1000 * 60)) % 60);
        long hours = (long) ((totTime / (1000 * 60 * 60)) % 24);

        if (hours >= 1) {
            timeStr = (hours + " hours " + minutes + " mins " + seconds + " secs");
        } else if (minutes >= 1) {
            timeStr = (minutes + " mins " + seconds + " secs");
        } else {
            timeStr = (seconds + " secs");
        }
        return timeStr;
    }

    static void executeNSGAII(String outPath) {
        long start = System.currentTimeMillis();
        List<NondominatedPopulation> results;

        System.out.println("\nNSGAII Parameters:");
        System.out.println("Operator: hux+bf");
        System.out.println("populationSize: " + popSize);
        System.out.println("MaxEvaluations: " + maxEval + "\n");

        // if single run create intermediate result
        if (nRuns == 1) {
            Problem problem = null;
            Algorithm algorithm = null;
            ResultFileWriter resWrite = null;

            // Declare algorithm properties:
            final Properties properties = new Properties();
            // Set the population size:
            properties.setProperty("operator", "hux+bf");
            properties.setProperty("populationSize", ("" + popSize));

            long startTime = System.currentTimeMillis();

            try {
                problem = new org.eocframework.problem.eoc.ObjMCCDiversity(baseClsf.length, nFeatures, isMultiClass, kFold, true);
                algorithm = AlgorithmFactory.getInstance().getAlgorithm("NSGAII", properties, problem);

                try {
                    String resFname = (strPath + "/" + relName + "-IntermRes.set");
                    resWrite = new ResultFileWriter(problem, new File(resFname));

                    // run algorithm
                    while (!algorithm.isTerminated() && (algorithm.getNumberOfEvaluations() < maxEval)) {
                        algorithm.step();   //run one generation of the algorithm

                        TypedProperties prop = new TypedProperties();
                        prop.setInt("NFE", algorithm.getNumberOfEvaluations());
                        long totTime = (System.currentTimeMillis() - start);
                        String timeStr = longToStringTime(totTime);

                        prop.setString("RunTime", timeStr);
                        System.out.println(prop.getProperties().toString());

                        resWrite.append(new ResultEntry(algorithm.getResult(), prop));
                        start = System.currentTimeMillis();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    if (resWrite != null) {
                        resWrite.close();
                    }
                }
            } finally {
                if (problem != null) {
                    problem.close();
                }
            }
        } else {
            // Website Help: http://keyboardscientist.weebly.com/blog/defining-termination-conditions
            results = new Executor()
                    .withProblemClass(ObjMCCDiversity.class, baseClsf.length, nFeatures, isMultiClass, kFold)
                    .withAlgorithm("NSGAII")
                    .withProperty("operator", "hux+bf")
                    .withMaxEvaluations(maxEval)
                    .withProperty("populationSize", popSize)
                    .distributeOnAllCores()
                    .runSeeds(nRuns);

            System.out.println("Run Completed : " + results.size());
            long totTime = (System.currentTimeMillis() - start);
            System.out.println("Total elapsed time: " + longToStringTime(totTime));
            System.out.println("\nNSGAII Solutions:\n");

            System.out.println("Run\tIndiv\tMCC\tDiversity");
            List<Double> obj1s = new ArrayList<Double>();
            List<Double> obj2s = new ArrayList<Double>();
            List<String> solns = new ArrayList<String>();
            List<Integer> runs = new ArrayList<Integer>();

            // writing Solutions to standard output
            int run = 0;
            for (NondominatedPopulation res : results) {
                run++;
                for (Solution solution : res) {
                    String strSoln = solution.getVariable(0).toString();
                    double obj1 = -(solution.getObjective(0));
                    double obj2 = -(solution.getObjective(1)); // * 5.0);
                    if (Double.isNaN(obj2) == false) {
                        runs.add(run);
                        solns.add(strSoln);
                        obj1s.add(obj1);
                        obj2s.add(obj2);
                        System.out.println(run + "\t\"" + strSoln + "\"\t" + obj1 + "\t" + obj2);
                    }
                }
            }

            // writing outputs into files
            Writer writer = null;
            int idx = 0;

            // Write Solution into VAR File
            String fname = (strPath + "/" + relName + "-VAR");

            try {
                writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fname), "utf-8"));

                for (String strSoln : solns) {
                    writer.write(runs.get(idx) + "\t\"" + strSoln + "\"\n");
                    idx++;
                }
                writer.close();
            } catch (IOException ex) {
                System.err.println("Unable to Create Solution Arff File: " + fname);
                //                        Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
            }

            // Write Objectives into FUN file
            fname = (strPath + "/" + relName + "-FUN");
            for (String strSoln : solns) {
                try {
                    writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fname), "utf-8"));
                    idx = 0;
                    for (int rn : runs) {
                        writer.write(rn + "\t" + obj1s.get(idx) + "\t" + obj2s.get(idx) + "\n");
                        idx++;
                    }
                    writer.close();
                } catch (Exception ex) {
                    System.err.println("Unable to write Obj Values into: " + fname);
                    //Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            // Create Solution ARFF file
            for (String strSoln : solns) {
                String strFSInfo = strSoln.substring(0, fsLen + pctLen);
                String dataFileName = strPath + "/" + relName + "-" + strFSInfo + ".arff";
                try {
                    Instances solData = getTopNFeatureData(strFSInfo, nFeatures);
                    saveArff(solData, dataFileName);
                } catch (IOException ex) {
                    System.err.println("Unable to Create Solution Arff File: " + dataFileName);
                    //                        Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            // }// end multi execution

        }

        /*if (globalDebug == true) {
         Instrumenter instrumenter = new Instrumenter()
         .withProblemClass(ObjMCCDiversity.class, baseClsf.length, nFeatures, isMultiClass, kFold)
         .withFrequency(10)
         .attachPopulationSizeCollector()
         .

         Executor executor = new Executor()
         .withProblemClass(ObjMCCDiversity.class, baseClsf.length, nFeatures, isMultiClass, kFold)
         .withAlgorithm("NSGAII")
         .withProperty("operator", "hux+bf")
         .withMaxEvaluations(maxEval)
         .withProperty("populationSize", popSize)
         .withInstrumenter(instrumenter);
         //.distributeOnAllCores();

         NondominatedPopulation result = executor.run();

         // print the runtime dynamics
         System.out.format("  NFE    Popsize%n");
         Accumulator accumulator = instrumenter.getLastAccumulator();
         for (int i = 0; i < accumulator.size("NFE"); i++) {
         System.out.println(accumulator.get("NFE", i) + "\t" + accumulator.get("Population Size", i));
         }

         } else {*/
        //kFold=10;
    }

    static void loadCSVResult(String csvFile) {
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = "\t";
        int i = 0;

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                // use comma as separator
                if (line.isEmpty() == true) {
                    break;
                }
                String[] result = line.split(cvsSplitBy);
                run.add(result[0]);
                String strInd = result[1];
                if (strInd.charAt(0) == '\"') {
                    strInd = strInd.substring(1, strInd.length() - 1);    //trim the double quotes from both side of the string
                }
                fullCMB.add(strInd);
                ensmCmb.add(strInd.substring(10));
                //mcc.add(result[2]);
                //div.add(result[3]);
                i++;
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Line No: " + i);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Reading Result File: Done!");
    }

    public static Evaluation crossvalidateEoCown(String cmb, Instances trn) throws Exception {
        // evaluate EOC and get mcc
        EvaluateMOEnsembleObjs evalEoC = new EvaluateMOEnsembleObjs(cmb,  isMultiClass);
        Evaluation eval = evalEoC.evaluateCVCombinationEoC(trn);
        return eval;
    }

    public static Evaluation evaluateEoCown(String cmb, Instances trn, Instances tst) throws Exception {
        // evaluate EOC and get mcc
        EvaluateMOEnsembleObjs evalEoC = new EvaluateMOEnsembleObjs(cmb,  isMultiClass);
        Evaluation eval = evalEoC.evaluateCombinationEoC(trn, tst);
        return eval;
    }

    public static void readCmdOptions(String[] args) throws Exception {
        String strRuns = "";
        String strFltr = "";

        arffFile = Utils.getOption("t", args);
        System.out.println("Train File: " + arffFile);

        // Test option
        tstFile = Utils.getOption("T", args);
        if (!tstFile.isEmpty()) {
            System.out.println("Test File: " + tstFile);
            isTest = true;
        }

        String mlt = Utils.getOption("M", args);
        if (!mlt.isEmpty()) {
            System.out.println("Multi-class: true ");
            isMultiClass = true;
        }

        strPath = Utils.getOption("o", args);
        if (!strPath.isEmpty()) {
            System.out.println("Output Path: " + strPath);
        }

        resFile = Utils.getOption("i", args);
        if (!resFile.isEmpty()) {
            System.out.println("Result File: " + resFile);
            isRes = true;
        }

        String strCV = Utils.getOption("k", args);
        isCV = false;

        if (!strCV.isEmpty()) {
            System.out.println("k-fold Cross Validation: " + strCV);
            kFold = Integer.parseInt(strCV);
            isCV = true;
        }

        String strMaxEval = Utils.getOption("e", args);
        if (!strMaxEval.isEmpty()) {
            System.out.println("Maximum Evaluation: " + strMaxEval);
            maxEval = Integer.parseInt(strMaxEval);
        }

        String strPopSize = Utils.getOption("p", args);
        if (!strPopSize.isEmpty()) {
            System.out.println("Population Size: " + strPopSize);
            popSize = Integer.parseInt(strPopSize);
        }

        strRuns = Utils.getOption("r", args);
        if (!strRuns.isEmpty()) {
            System.out.println("Repeated Runs: " + strRuns);
            nRuns = Integer.parseInt(strRuns);
        } else {
            nRuns = 1;
        }
        strFltr = Utils.getOption("f", args);
        isFilter = false;
        if (!strFltr.isEmpty()) {
            System.out.println("Preprocessing Filtering Enabled: " + strFltr);
            if (strFltr.equalsIgnoreCase("t") == true) {
                isFilter = true;
            }
        }

        // Debug Enable
        // Test option
        String dbg = Utils.getOption("D", args);
        if (!dbg.isEmpty()) {
            System.out.println("Global Debug: Enabled ");
            globalDebug = true;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        // output usage
        if (args.length == 0) {
            System.err.println("\nUsage: java -jar MO-EoC-Wrapper.jar\n\t"
                    + "-t <training arff file>\n\t"
                    + "-T <Testing arff file>\n\t"
                    + "-o <output path>\n\t"
                    + "-M <true : Multi-class dataset>\n\t"
                    + "-r <no of repeated runs. Default = 1>\n\t"
                    + "-e <maximum evaluation. Default 10000>\n\t"
                    + "-p <population size. Default 100>\n\t"
                    + "-i <input result file path for validation of eoc>\n\t"
                    + "-f <true : filtering as pre-processing>\n\t"
                    + "-k <k-fold cross-validation>\n\t"
                    + "-D <true: enable global debug>\n");
            System.exit(1);
        } else {
            readCmdOptions(args);
        }

        // Load Training data        
        trnData = loadArff(arffFile);
        relName = trnData.relationName();
        nFeatures = trnData.numAttributes();
        nClass = trnData.numClasses();
        if (isMultiClass == false && nClass > 2) {
            System.out.println("\n\nThis version with 29 base classifiers for MO-EoC-Wrapper supports binary-class data."
                    + "If you would like to run it for 25 base classifiers compatible for multi-class problem\n"
                    + "please run with <-M true> option.\n");
            System.exit(0);
        } else if (isMultiClass == true && nClass == 2) {
            System.out.println("\n\nThe dataset is binary-class.\n"
                    + "please remove <-M true> option.\n");
            System.exit(0);
        }

        if (nClass > 2) {
            baseClsf = baseClassifiersMultiClass;
        } else {
            baseClsf = baseClassifiers;
        }

        // if both the test dataset and result file given
        if (isTest == true && isRes == true) {
            tstData = loadArff(tstFile);

            loadCSVResult(resFile);
            int idx = 0;
            String nRun = "";
            Writer glbWriter = null;

            if (globalDebug == true) {
                String fileName = strPath + "/" + relName + "-eval.D.txt";
                System.out.println("Global Debug Output: " + fileName);
                try {
                    glbWriter = new BufferedWriter(new OutputStreamWriter(
                            new FileOutputStream(fileName), "utf-8"));
                    glbWriter.write("Run\tSolution\tFSName\t#Features\tMCC\tAcc\tPrec\tF-Measure\tAUC\tSEN\tSPEC\n");
                } catch (IOException ex) {
                    // report
                }
            }
            System.out.println("Run\tSolution\tFSName\t#Features\tMCC\tAcc\tPrec\tF-Measure\tAUC\tSEN\tSPEC");
            for (String solnCmb : fullCMB) {

                String strFSInfo = solnCmb.substring(0, fsLen + pctLen);
                String fileBuilder = relName + "-" + strFSInfo + ".arff";
                String trainFile = strPath + "/" + fileBuilder;
                String strCmb = ensmCmb.get(idx);
                String fs = solnCmb.substring(0, fsLen);
                int intFS = Integer.parseInt(fs, 2);
                String fsName = getFSName(intFS);
                nRun = run.get(idx);
                idx++;

                Instances solnTrnData = null;
                File f = new File(trainFile);
                if (!f.exists()) {
                    System.err.println("Warning: File does not exist: " + trainFile + ".\tTrying to build the file.");
                    if (fsLoaded == false) {
                        // do the Features Ranking
                        initFeatureRanks(trnData);
                        fsLoaded = true;
                    }
                    try {
                        solnTrnData = getTopNFeatureData(strFSInfo, nFeatures);
                        saveArff(solnTrnData, trainFile);
                    } catch (IOException ex) {
                        System.err.println("Unable to Create Solution Arff File: " + trainFile);
//                        Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    solnTrnData = loadArff(trainFile);
                }

                int nfeat = solnTrnData.numAttributes() - 1;
                /*
                 Instances newTstData = trainTestCompatible(solnTrnData, tstData);
               
                 fileBuilder = relName + "-" + strFSInfo + "-Tst.arff";
                 trainFile = strPath + fileBuilder;
                 saveArff(newTstData, trainFile);
                 */

                Evaluation eval = evaluateEoCown(strCmb, solnTrnData, tstData);
                if (eval == null) {
                    String strOut = nRun + "\t" + solnCmb + "\t" + fsName + "\t" + nfeat;
                    System.out.println(strOut);
                    if (globalDebug == true) {
                        glbWriter.append(strOut + "\n");
                        glbWriter.append("Unable ot evaluate: " + solnCmb);
                    }
                } else {
                    String evalRes = getEvaluationResult(eval);
                    String strOut = nRun + "\t" + solnCmb + "\t" + fsName + "\t" + nfeat + "\t" + evalRes;
                    System.out.println(strOut);
                    if (globalDebug == true) {
                        glbWriter.append(strOut + "\n");
                        glbWriter.append(eval.toMatrixString());
                    }
                }
                /*                
                 eval = evaluateEoCown(strCmb, solnTrnData, newTstData);
                 if (eval == null) {
                 String strOut = nRun + "\t" + solnCmb + "\t" + fsName + "\t" + nfeat;
                 System.out.println(strOut);
                 if (globalDebug == true) {
                 glbWriter.append(strOut + "\n");
                 glbWriter.append("Unable ot evaluate: " + solnCmb);
                 }
                 } else {
                 String evalRes = getEvaluationResult(eval);
                 String strOut = "Own:\t" + nRun + "\t" + solnCmb + "\t" + fsName + "\t" + nfeat + "\t" + evalRes;
                 System.out.println(strOut);
                 if (globalDebug == true) {
                 glbWriter.append(strOut + "\n");
                 glbWriter.append(eval.toMatrixString());
                 }
                 }
                 */
            }
            if (globalDebug == true) {
                //Instances newTstData = trainTestCompatible(trnData, tstData);
                String strBsClsf = evaluateBaseClassifiers(baseClsf, trnData, tstData);
                System.out.println(strBsClsf);
                glbWriter.append(strBsClsf);

                strBsClsf = evaluateWekaEoC(wekaEoC, trnData, tstData);
                System.out.println(strBsClsf);
                glbWriter.append(strBsClsf);
                glbWriter.close();
            }
            System.exit(0);

        } else if (isRes == true) {
            // if only result file given; no test file given. Will do 10-fold CV
            loadCSVResult(resFile);

            int idx = 0;
            String nRun = "";
            Writer writer = null;

            if (globalDebug == true) {
                String fileName = strPath + "/" + relName + "-eval.D.txt";
                System.out.println("Global Debug Output: " + fileName);

                try {
                    writer = new BufferedWriter(new OutputStreamWriter(
                            new FileOutputStream(fileName), "utf-8"));
                    writer.write("Run\tSolution\tFSName\t#Features\tMCC\tAcc\tPrec\tF-Measure\tAUC\tSEN\tSPEC\n");
                } catch (IOException ex) {
                    // report
                }
            }
            //System.out.println("Solution\tFSName\t#Features\tMCC\tAcc\tPrec\tF-Measure\tAUC");
            System.out.println("Run\tSolution\tFSName\t#Features\tMCC\tAcc\tPrec\tF-Measure\tAUC\tSEN\tSPEC");

            for (String solnCmb : fullCMB) {
                String strFSInfo = solnCmb.substring(0, fsLen + pctLen);
                String fileBuilder = relName + "-" + strFSInfo + ".arff";
                String strCmb = ensmCmb.get(idx);
                String trainFile = strPath + "/" + fileBuilder;
                nRun = run.get(idx);
                idx++;

                String fs = solnCmb.substring(0, fsLen);
                int intFS = Integer.parseInt(fs, 2);
                String fsName = getFSName(intFS);

                //System.out.println(trainFile);
                Instances trnData = null;
                File f = new File(trainFile);
                if (!f.exists()) {
                    System.err.println("Warning: File does not exist: " + trainFile + ".\tTrying to build the file.");
                    if (fsLoaded == false) {
                        // do the Features Ranking
                        initFeatureRanks(trnData);
                        fsLoaded = true;
                    }
                    try {
                        trnData = getTopNFeatureData(strFSInfo, nFeatures);
                        saveArff(trnData, trainFile);
                    } catch (IOException ex) {
                        System.err.println("Unable to Create Solution Arff File: " + trainFile);
//                        Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    trnData = loadArff(trainFile);
                }

                int nfeat = trnData.numAttributes() - 1;
                /*
                 EvaluateMOEnsembleObjs testEoC = new EvaluateMOEnsembleObjs(trnData, strCmb, "CV", isMultiClass);
                 testEoC.crossValidEvalEoC();
                 Evaluation eval = testEoC.getEvaluation();
                 */
                //evaluateEoCown
                Evaluation eval = crossvalidateEoCown(strCmb, trnData);
                String evalRes = getEvaluationResult(eval);

                String strOut = nRun + "\t" + solnCmb + "\t" + fsName + "\t" + nfeat + "\t" + evalRes;
                System.out.println(strOut);
                if (globalDebug == true) {
                    //System.out.println(eval.toMatrixString());
                    writer.append(strOut + "\n");
                    writer.append(eval.toMatrixString());
                }
            }
            if (globalDebug == true) {
                String strBsClsf = tenFoldBaseClassifiers(baseClsf, trnData);
                System.out.println(strBsClsf);
                writer.append(strBsClsf);

                strBsClsf = tenFoldWekaEoC(wekaEoC, trnData);
                System.out.println(strBsClsf);
                writer.append(strBsClsf);
                writer.close();
            }

            System.exit(0);

        } else {
            System.out.println("Dataset:" + relName + "\tFeatures: " + nFeatures + "\t Samples:" + trnData.numInstances());

// do filtering if enabled 
            if (isFilter == true) {
                trnData = reduceDimensionality(trnData);
                trnData.setRelationName(relName);
                nFeatures = trnData.numAttributes();
                String redFile = (strPath + "/" + relName + "-Reduc-Dimens-Cfs.arff");
                System.out.println("Reduced Features: " + nFeatures + "\t Samples:" + trnData.numInstances() + "\nWritten in: " + redFile);
                saveArff(trnData, redFile);
            }

// do the Features Ranking
            initFeatureRanks(trnData);
// Individual 
            /*String cmb="001000001001010000000000000000000000000";
             ObjMCCDiversity indiv = new ObjMCCDiversity(baseClsf.length,nFeatures,false,10);
             Solution soln = indiv.setSolution(cmb);
             indiv.evaluate(soln);
             System.out.println((-soln.getObjective(0))+"\t"+(-soln.getObjective(1)));
             */
// execute NSGAII algorithm  

            executeNSGAII(strPath);
            System.out.println("Done!");

            File file = new File(strPath + "/" + relName + "-" + nRuns + "-run-done");
            file.createNewFile();
        }

        System.exit(0);
    }

    static String getEvaluationResult(Evaluation eval) {
        double mcc = (eval.weightedMatthewsCorrelation());
        double acc = (eval.pctCorrect());
        double prec = eval.weightedPrecision();
        double fm = eval.weightedFMeasure();
        double sen = eval.weightedTruePositiveRate();
        double spec = eval.weightedTrueNegativeRate();
        double auc = (eval.weightedAreaUnderROC());
        String res = mcc + "\t" + acc + "\t" + prec + "\t" + fm + "\t" + auc + "\t" + sen + "\t" + spec;
        return res;
    }
}

/// -t /home/mohammad/MO-EoC_Wraper-Parallel/InputDataset/NISP2003/arcene_train-EntF.arff -f /home/mohammad/MO-EoC_Wraper-Parallel/NIPS-MO-EoC_Wrapper-arcene.txt -T /home/mohammad/MO-EoC_Wraper-Parallel/InputDataset/NISP2003/arcene_test.arff -o /home/mohammad/MO-EoC_Wraper-Parallel/OutputDataset/NIPS2003/
// -t /home/mohammad/MO-EoC_Wraper-Parallel/InputDataset/NISP2003/arcene_train-EntF.arff -o /home/mohammad/MO-EoC_Wraper-Parallel/OutputDataset/NIPS2003/
//
// Evaluation Test : Binary Class 
// -t /home/mohammad/Desktop/Mohammad-Final-Exp/inputDataset/onlyTraining/breast-entf.arff -o /home/mohammad/Desktop/Mohammad-Final-Exp/outputDataset/onlyTraining/ -i /home/mohammad/Desktop/Mohammad-Final-Exp/outputDataset/onlyTraining/breast-entf-VAR
// -t /home/mohammad/Desktop/Mohammad-Final-Exp/inputDataset/trainTest/arcene-train-EntF.arff -T /home/mohammad/Desktop/Mohammad-Final-Exp/inputDataset/trainTest/arcene_test.arff -o /home/mohammad/Desktop/Mohammad-Final-Exp/outputDataset/trainTest/ -i /home/mohammad/Desktop/Mohammad-Final-Exp/outputDataset/trainTest/arcene-train-EntF-VAR -D true
// Test MultiClass Feature Selection
// -t /home/mohammad/Dataset/MultiClass/iris.arff -o /home/mohammad/Dataset/MultiClass/ -f 0 -n 4
//
// Multi Class Feature Selection
// -t /home/mohammad/Dataset/MultiClass/column_3C_weka.arff -o /home/mohammad/Dataset/MultiClass/ -f 0 -n 6

/*


 public static Evaluation crossValidateEoC(String cmb, Instances trn) {
 Vote majVoteEoC = new Vote();
 String opts[][] = new String[cmb.length()][2];
 int oid = 0;

 for (int i = 0; i < cmb.length(); i++) {
 if (cmb.charAt(i) == '1') {
 //baseClassifiers
 String clsName = "";
 clsName = baseClsf[i].getClass().getCanonicalName();
 opts[oid][0] = "-B";
 opts[oid][1] = clsName;
 oid++;
 }
 }
 opts[oid][0] = "-R";
 opts[oid][1] = "MAJ";
 oid++;

 int nRes = oid, vid = 0;
 String[] vOpt = new String[oid * 2];
 for (oid = 0; oid < nRes; oid++, vid++) {
 vOpt[vid] = opts[oid][0];
 vid++;
 vOpt[vid] = opts[oid][1];
 }
 try {
 majVoteEoC.setOptions(vOpt);
 } catch (Exception ex) {
 System.err.println("Unable to Set options for MO-EoC Majority Vote....");
 //Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
 }

 // evaluate EOC and get mcc
 Evaluation eval = null;
 try {
 eval = new Evaluation(trn);
 eval.crossValidateModel(majVoteEoC, trn, 10, new Random(1));
 } catch (Exception ex) {
 System.err.println("Unable to evaluate the MO-EoC Majority Vote....");
 //Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
 }
 return eval;
 }

 public static Evaluation evaluateEoC(String cmb, Instances trn, Instances tst) {
 Vote majVoteEoC = new Vote();
 String opts[][] = new String[cmb.length()][2];
 int oid = 0;

 for (int i = 0; i < cmb.length(); i++) {
 if (cmb.charAt(i) == '1') {
 //baseClassifiers
 String clsName = "";
 clsName = baseClsf[i].getClass().getCanonicalName();
 opts[oid][0] = "-B";
 opts[oid][1] = clsName;
 oid++;
 }
 }
 opts[oid][0] = "-R";
 opts[oid][1] = "MAJ";
 oid++;

 int nRes = oid, vid = 0;
 String[] vOpt = new String[oid * 2];
 for (oid = 0; oid < nRes; oid++, vid++) {
 vOpt[vid] = opts[oid][0];
 vid++;
 vOpt[vid] = opts[oid][1];
 }
 try {
 majVoteEoC.setOptions(vOpt);
 } catch (Exception ex) {
 System.err.println("Unable to Set options for MO-EoC Majority Vote....");
 //Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
 }
 try {
 majVoteEoC.buildClassifier(trn);
 } catch (Exception ex) {
 System.err.println("Unable to build base classifiers for MO-EoC Majority Vote....");
 Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
 }

 // evaluate EOC and get mcc
 Evaluation eval = null;
 try {
 eval = new Evaluation(trn);
 eval.evaluateModel(majVoteEoC, tst);
 } catch (Exception ex) {
 System.err.println("Unable to evaluate the MO-EoC Majority Vote....");
 //Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
 }
 return eval;
 }

 */
