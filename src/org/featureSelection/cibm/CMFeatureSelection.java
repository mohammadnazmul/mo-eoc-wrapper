/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.featureSelection.cibm;

import java.util.ArrayList;
import java.util.List;
import org.featureSelection.cibm.cm.CMScoreAttributeEval;
import static org.functionlib.wekaFunctions.retainFeatures;
import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.Ranker;
import weka.core.Instances;

/**
 *
 * @author mohammad 1) Instantiate an object objFS = new
 * CMFeatureSelection(Instances arffData); 2) call generateRanks(); 3) Instances
 * data = getArffWithTopFeatures(int fsMethod, double percnt);
 */
public class CMFeatureSelection {

    public boolean isDebug = true;
    Instances inputData;
    String dataPath;
    int numberOfFeatures;
    int numberOfClasses;
    int clsIdx;
    String relName;

    // holds rank of features
    double[][] ranksCM1;
    double[][] ranksCM2;

    // holds rank of features
    ArrayList<double[][]> mcRanksCM1 = new ArrayList<double[][]>();
    ArrayList<double[][]> mcRanksCM2 = new ArrayList<double[][]>();

    public CMFeatureSelection(Instances arffData) {
        inputData = arffData;
        numberOfFeatures = inputData.numAttributes() - 1;
        numberOfClasses = inputData.numClasses();
        clsIdx = inputData.classIndex() + 1;
        relName = arffData.relationName();
    }

    public void generateRanks() {
        String nRank = "" + numberOfFeatures;
        String[] options = {"-T", "-1.7976931348623157E308", "-N", nRank};

        if (numberOfClasses == 2) {
            ranksCM1 = FSCMScore(1, options);
            ranksCM2 = FSCMScore(2, options);
        } else {
            for (int ci = 0; ci < numberOfClasses; ci++) {
                double[][] cm1Rank = FSCMScoreMC(1, ci, options);
                mcRanksCM1.add(cm1Rank);
                double[][] cm2Rank = FSCMScoreMC(2, ci, options);
                mcRanksCM2.add(cm2Rank);
            }
        }
    }

    private String genIndexString(double[][] ranks, int nTop) {
        int topPos = 0;
        int mid = nTop / 2;
        if (nTop % 2 == 1) {
            topPos = mid + 1;
        } else {
            topPos = mid;
        }
        int startNeg = ranks.length - mid;

        String indices = "";
        for (int i = 0; i < topPos; i++) {
            int idx = ((int) ranks[i][0] + 1);
            if (idx != clsIdx) {
                indices += (idx + ",");
            }
        }

        for (int i = startNeg; i < ranks.length; i++) {
            int idx = ((int) ranks[i][0] + 1);
            if (idx != clsIdx) {
                indices += (idx + ",");
            }
        }
        indices += clsIdx;
        return indices;
    }

    private String genIndexStringMC(ArrayList<double[][]> ranksMC, int nTop) {
        List<Integer> idxList = new ArrayList<Integer>();
        int perClass = nTop / numberOfClasses;
        int posNegTotal = perClass / 2;
//        int clid = 0;

        for (double[][] ranks : ranksMC) {

            int posCntr = 0;
            int cmIdx = 0;
            while (posCntr < posNegTotal) {
                int idx = ((int) ranks[cmIdx][0]) + 1;
                if (!idxList.contains(idx)) {
                    idxList.add(idx);
                    posCntr++;
                }
                cmIdx++;
            }

            int negCntr = 0;
            int negIdx = ranks.length - 1;
            while ((negCntr < posNegTotal) && (negIdx > cmIdx)) {
                int idx = ((int) ranks[negIdx][0]) + 1;
                if (!idxList.contains(idx)) {
                    idxList.add(idx);
                    negCntr++;
                }
                negIdx--;
            }
//            System.out.println("Class : " + clid + "\tFeature: " + idxList.size() );
//            clid++;
        }

        String indices = "";
        for (int id : idxList) {
            indices += (id + ",");
        }
        indices += clsIdx;
        return indices;
    }

    /*
     private String genIndexStringMC(ArrayList<double[][]> ranksMC, int nTop) {
     List<Integer> idxList = new ArrayList<Integer>();
     int perClass = nTop / numberOfClasses;

     for (double[][] ranks : ranksMC) {
     int posTotal = perClass / 2;
     int pos = 0;
     int total = 0;
     while (total < posTotal) {
     int idx = ((int) ranks[pos][0] + 1);
     if (!idxList.contains(idx)) {
     idxList.add(idx);
     total++;
     }
     pos++;
     }

     int negPos = ranks.length-1;
     while ((total < perClass) || (negPos == pos)) {
     int idx = ((int) ranks[negPos][0] + 1);
     if (!idxList.contains(idx)) {
     idxList.add(idx);
     total++;
     }
     negPos--;
     }
     }
     String indices = "";
     for (int id : idxList) {
     indices += (id + ",");
     }
     indices += clsIdx;
     return indices;
     }
     /*
     for (double[][] ranks : ranksMC) {
          
     int topPos = 0;
     int mid = perClass / 2;
     if (perClass % 2 == 1) {
     topPos = mid + 1;
     } else {
     topPos = mid;
     }
     int startNeg = ranks.length - mid;

     for (int i = 0; i < topPos; i++) {
     int idx = ((int) ranks[i][0] + 1);
     if (idx != clsIdx) {
     if (!idxList.contains(idx)) {
     idxList.add(idx);
     } else {
     i++;
     idx = ((int) ranks[i][0] + 1);
     if (!idxList.contains(idx)) {
     idxList.add(idx);
     }
     }
     //indices += (idx + ",");
     }
     }

     for (int i = startNeg; i < ranks.length; i++) {
     int idx = ((int) ranks[i][0] + 1);
     if (idx != clsIdx) {
     if (!idxList.contains(idx)) {
     idxList.add(idx);
     }
     else
     {
     i++;
     idx = ((int) ranks[i][0] + 1);
     if (!idxList.contains(idx)) {
     idxList.add(idx);
     }
     }
     //indices += (idx + ",");
     }
     }
     }
      
     }
     */
    public Instances getArffWithTopFeatures(int fsMethod, double percnt) throws Exception {
        int numberOfSelFeatures = (int) (numberOfFeatures * percnt);
        Instances selFeatureData = getNFeatures(fsMethod, numberOfSelFeatures);
        selFeatureData.setRelationName(relName);

        return selFeatureData;
    }

    public Instances getArffWithTopFeatures(int fsMethod, int nFeatures) throws Exception {
        Instances selFeatureData = getNFeatures(fsMethod, nFeatures);
        selFeatureData.setRelationName(relName);

        return selFeatureData;
    }

    Instances getNFeatures(int fsMethod, int nFeature) throws Exception {
        Instances selFeatureData = null;
        int numberOfSelFeatures = nFeature;
        String indices = "";

        switch (fsMethod) {
            case 0:                // strFSName = "CM1";
                if (numberOfClasses == 2) {
                    indices = genIndexString(ranksCM1, numberOfSelFeatures);
                } else {
                    indices = genIndexStringMC(mcRanksCM1, numberOfSelFeatures);
                }

                selFeatureData = retainFeatures(inputData, indices);
                break;
            case 1:                // strFSName = "CM2";
                if (numberOfClasses == 2) {
                    indices = genIndexString(ranksCM2, numberOfSelFeatures);
                } else {
                    indices = genIndexStringMC(mcRanksCM2, numberOfSelFeatures);
                }
                selFeatureData = retainFeatures(inputData, indices);
                break;
        }
        // Set Class Index
        int cid = selFeatureData.numAttributes() - 1;
        selFeatureData.setClassIndex(cid);
        selFeatureData.setRelationName(relName);
        return selFeatureData;
    }

    protected double[][] FSCMScore(int cmN, String[] rankSearchOptions) {
        AttributeSelection attSelection = new AttributeSelection();
        String nf = "" + numberOfFeatures;
        String cm = "" + cmN;

        // Step 1: setEvaluator
        String[] cmOptions = {"-C", cm, "-N", nf};
        CMScoreAttributeEval eval = new CMScoreAttributeEval();

        eval.setOptions(cmOptions);
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        try {
            rank.setOptions(rankSearchOptions);
        } catch (Exception ex) {
            System.err.println(CMFeatureSelection.class.getName() + " : CM Score Attribute Evaluation Search Error");
        }
        attSelection.setSearch(rank);
        try {
            attSelection.SelectAttributes(inputData);
        } catch (Exception ex) {
            System.err.println(CMFeatureSelection.class.getName() + " : Unable to select attribute");
        }

        double[][] ranks = new double[numberOfFeatures][numberOfClasses];
        try {
            ranks = attSelection.rankedAttributes();
        } catch (Exception ex) {
            System.err.println(CMFeatureSelection.class.getName() + " : Unable to Rank Features");
        }
        return ranks;
    }

    protected double[][] FSCMScoreMC(int cmN, int clsIdx, String[] rankSearchOptions) {
        AttributeSelection attSelection = new AttributeSelection();
        String nf = "" + numberOfFeatures;
        String cm = "" + cmN;
        String ci = "" + clsIdx;
        // Step 1: setEvaluator
        String[] cmOptions = {"-C", cm, "-N", nf, "-i", ci};

        CMScoreAttributeEval eval = new CMScoreAttributeEval();

        eval.setOptions(cmOptions);
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        try {
            rank.setOptions(rankSearchOptions);
        } catch (Exception ex) {
            System.err.println(CMFeatureSelection.class.getName() + " : CM Score Attribute Evaluation Search Error");
        }
        attSelection.setSearch(rank);
        try {
            attSelection.SelectAttributes(inputData);
        } catch (Exception ex) {
            System.err.println(CMFeatureSelection.class.getName() + " : Unable to select attribute");
        }

        double[][] ranks = new double[numberOfFeatures][numberOfClasses];

        try {
            ranks = attSelection.rankedAttributes();
        } catch (Exception ex) {
            System.err.println(CMFeatureSelection.class.getName() + " : Unable to Rank Features");
        }
        return ranks;
    }
}
