/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.featureSelection.cibm.cm;

import java.util.*;
import weka.core.Instances;

/**
 *
 * @author mohammad
 */
public class CMScoreCalculator {

    public boolean isDebug = false;

    private double featClassSum[][];
    private String[] featureNames;
    private double minVal[][];
    private double maxVal[][];
    private double Data[][];
    private int clsIdx[];

    private String[] classLabels;
    private int[] clsCount;
    int numFeatures;
    int numClasses;
    int numSamples;
    double[][] m_cm1Scores;
    double[][] m_cm2Scores;
    int topNfeature;

    List<CMScoreObject> cm1Score = new ArrayList<CMScoreObject>();
    List<CMScoreObject> cm2Score = new ArrayList<CMScoreObject>();

    CMScoreCalculator() {
        throw new UnsupportedOperationException("Not supported yet.\n Need to provide an Arff data Instance."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @param data input arff data instances
     */
    public CMScoreCalculator(Instances data, boolean isDebug) {
        numClasses = data.numClasses();
        numFeatures = (data.numAttributes() - 1);
        numSamples = data.numInstances();
        this.isDebug = isDebug;

        // Getting Class Index for all Instances
        clsCount = new int[numClasses];
        Arrays.fill(clsCount, 0);
        clsIdx = new int[numSamples];
        for (int i = 0; i < numSamples; i++) {
            int clsVal = (int) data.instance(i).classValue();
            clsIdx[i] = clsVal;
            clsCount[clsVal]++;
        }
//        System.out.println("Class 0: " + clsCount[0] + "\tClass 1: " + clsCount[1]);

        // Getting Class Labels
        classLabels = new String[numClasses];
        for (int c = 0; c < numClasses; c++) {
            classLabels[c] = data.classAttribute().value(c);
        }
//        System.out.println("Class 0: " + classLabels[0] + "\tClass 1: " + classLabels[1]);

        // Getting value for all instances per feature
        Data = new double[numFeatures][numSamples];
        featClassSum = new double[numFeatures][numClasses];
        for (int i = 0; i < numFeatures; i++) {
            Arrays.fill(featClassSum[i], 0.0);
        }

        featureNames = new String[numFeatures];
        minVal = new double[numFeatures][numClasses];
        maxVal = new double[numFeatures][numClasses];
        for (int f = 0; f < numFeatures; f++) {
            double min[] = new double[numClasses];
            Arrays.fill(min, Double.MAX_VALUE);
            double max[] = new double[numClasses];
            Arrays.fill(max, Double.MIN_VALUE);

            featureNames[f] = data.attribute(f).name();

            for (int s = 0; s < numSamples; s++) {
                double val = data.instance(s).value(f);
                int clsPos = clsIdx[s];
                Data[f][s] = val;
                featClassSum[f][clsPos] += val;
                if (val < min[clsPos]) {
                    min[clsPos] = val;
                }
                if (val > max[clsPos]) {
                    max[clsPos] = val;
                }
            }
            for (int c = 0; c < numClasses; c++) {
                minVal[f][c] = min[c];
                maxVal[f][c] = max[c];
            }
        }

        if (isDebug == true) {
            for (int f = 0; f < numFeatures; f++) {
                System.out.print(data.attribute(f).name() + "\t");
                for (int c = 0; c < numClasses; c++) {
                    System.out.print(featClassSum[f][c] + "\t");
                }
                System.out.println();
                /*
                 System.out.print(data.attribute(f).name() + "\t");
                 for (int c = 0; c < numClasses; c++) {
                 System.out.print("(" + maxVal[f][c] + "," + minVal[f][c] + ")\t");
                 }
                 System.out.println();*/
            }
        }
    }

    public void CalculateCM1() {
        cm1Score.clear();

        m_cm1Scores = new double[numFeatures][numClasses];

        //for (int c = 0; c < numClasses; c++) {
        //double avg_target = 0.0, avg_others = 0.0, mins = Double.MAX_VALUE, maxs = Double.MIN_VALUE;
        for (int f = 0; f < numFeatures; f++) {
            for (int c = 0; c < numClasses; c++) {
                double avg_target = 0.0, avg_others = 0.0, mins = Double.MAX_VALUE, maxs = Double.MIN_VALUE;
                avg_target = featClassSum[f][c] / clsCount[c];
                int OclsCnt = 0;
                double Osum = 0.0;

                for (int ci = 0; ci < numClasses; ci++) {
                    if (ci != c) {
                        Osum += featClassSum[f][ci];
                        OclsCnt += clsCount[ci];

                        double val = minVal[f][ci];
                        if (val < mins) {
                            mins = val;
                        }
                        val = maxVal[f][ci];
                        if (maxs < val) {
                            maxs = val;
                        }
                    }
                } // endFor: ci

                avg_others = Osum / OclsCnt;
                double cmScore = (avg_target - avg_others) / (1 + Math.abs(maxs - mins));
                m_cm1Scores[f][c] = cmScore;
            } // endFor: c
            CMScoreObject score = new CMScoreObject(featureNames[f], f, m_cm1Scores[f]);
            cm1Score.add(score);
        }// endFor: f

        /*
         for (int f = 0; f < numFeatures; f++) {
         CMScoreObject score = new CMScoreObject(featureNames[f], f, m_cm1Scores[f]);
         cm1Score.add(score);
         }
        
         double avg_target = 0.0, avg_others = 0.0, mins = 0.0, maxs = 0.0;

         for (int f = 0; f < numFeatures; f++) {
         for (int c = 0; c < numClasses; c++) {
         if (c + 1 < numClasses) {
         avg_target = featClassSum[f][c] / clsCount[c];
         avg_others = featClassSum[f][c + 1] / clsCount[c + 1];
         mins = minVal[f][c + 1];
         maxs = maxVal[f][c + 1];
         } else {
         avg_target = featClassSum[f][c] / clsCount[c];
         avg_others = featClassSum[f][c - 1] / clsCount[c - 1];
         mins = minVal[f][c - 1];
         maxs = maxVal[f][c - 1];
         }
         m_cm1Scores[f][c] = (avg_target - avg_others) / (1 + Math.abs(maxs - mins));

         }
         score = new CMScoreObject(featureNames[f], f, m_cm1Scores[f]);
         cm1Score.add(score);
         }
         */
    }

    public void CalculateCM2() {
        cm2Score.clear();

        m_cm2Scores = new double[numFeatures][numClasses];

        //for (int c = 0; c < numClasses; c++) {
        //double avg_target = 0.0, avg_others = 0.0, mins = Double.MAX_VALUE, maxs = Double.MIN_VALUE;
        for (int f = 0; f < numFeatures; f++) {
            for (int c = 0; c < numClasses; c++) {
                double avg_target = 0.0, avg_others = 0.0, minsTarget = Double.MAX_VALUE, maxsTarget = Double.MIN_VALUE, minsOther = Double.MAX_VALUE, maxsOther = Double.MIN_VALUE;
                avg_target = featClassSum[f][c] / clsCount[c];
                int OclsCnt = 0;
                double Osum = 0.0;

                for (int ci = 0; ci < numClasses; ci++) {
                    if (ci != c) {
                        Osum += featClassSum[f][ci];
                        OclsCnt += clsCount[ci];

                        double val = minVal[f][ci];
                        if (val < minsOther) {
                            minsOther = val;
                        }
                        val = maxVal[f][ci];
                        if (maxsOther < val) {
                            maxsOther = val;
                        }
                    } else {
                        double val = minVal[f][ci];
                        if (val < minsTarget) {
                            minsTarget = val;
                        }
                        val = maxVal[f][ci];
                        if (maxsTarget < val) {
                            maxsTarget = val;
                        }
                    }
                } // endFor: ci
                double rangeTarget = Math.abs(maxsTarget - minsTarget);;
                double rangeOthers = Math.abs(maxsOther - minsOther);
                avg_others = Osum / OclsCnt;
                double cmScore = (avg_target - avg_others) / Math.min((1 + rangeTarget), (1 + rangeOthers));
                m_cm2Scores[f][c] = cmScore;
            } // endFor: c
            CMScoreObject score = new CMScoreObject(featureNames[f], f, m_cm2Scores[f]);
            cm2Score.add(score);
        }// endFor: f
    }
    /*
     public void CalculateCM2() {
     double avg_target = 0.0, avg_others = 0.0, mins_target = 0.0, maxs_target = 0.0;
     double rangeTarget = 0.0, mins_others = 0.0, maxs_others = 0.0, rangeOthers = 0.0;
     CMScoreObject score;
     cm2Score.clear();

     int numFeatures = featureNames.length;
     int numClasses = classLabels.length;
     m_cm2Scores = new double[numFeatures][numClasses];

     for (int f = 0; f < numFeatures; f++) {
     for (int c = 0; c < numClasses; c++) {
     if (c + 1 < numClasses) {
     avg_target = featClassSum[f][c] / clsCount[c];
     avg_others = featClassSum[f][c + 1] / clsCount[c + 1];

     mins_target = minVal[f][c];
     maxs_target = maxVal[f][c];
     rangeTarget = Math.abs(maxs_target - mins_target);

     mins_others = minVal[f][c + 1];
     maxs_others = maxVal[f][c + 1];
     rangeOthers = Math.abs(maxs_others - mins_others);
     } else {
     avg_target = featClassSum[f][c] / clsCount[c];
     avg_others = featClassSum[f][c - 1] / clsCount[c - 1];

     mins_target = minVal[f][c];
     maxs_target = maxVal[f][c];
     rangeTarget = Math.abs(maxs_target - mins_target);

     mins_others = minVal[f][c - 1];
     maxs_others = maxVal[f][c - 1];
     rangeOthers = Math.abs(maxs_others - mins_others);
     }
     m_cm2Scores[f][c] = (avg_target - avg_others) / Math.min((1 + rangeTarget), (1 + rangeOthers));
     }
     score = new CMScoreObject(featureNames[f], f, m_cm2Scores[f]);
     cm2Score.add(score);
     }
     }
     */
// Getter Methods

    public double[][] getCM1Scores() {
        return m_cm1Scores;
    }

    public List<CMScoreObject> getCM1ScoreObject() {
        return cm1Score;
    }

    public double[][] getCM2Scores() {
        return m_cm2Scores;
    }

    public List<CMScoreObject> getCM2ScoreObject() {
        return cm2Score;
    }

    public String[] getFeatures() {
        return featureNames;
    }

    public String[] getClasses() {
        return classLabels;
    }
// Print/Show Methods

    public void showCMScores(String name, List<CMScoreObject> cm) {
        System.out.print(name + " Scores....\nFeature");
        for (int c = 0; c < numClasses; c++) {
            System.out.print("\t" + classLabels[c]);
        }
        System.out.println();

        for (CMScoreObject anFeature : cm) {
            System.out.println(anFeature.toString());
        }
    }

    public void showCM1Scores() {
        System.out.print("\nCM1 Scores...\nFeature");
        for (int c = 0; c < numClasses; c++) {
            System.out.print("\t" + classLabels[c]);
        }
        System.out.println();

        for (CMScoreObject anFeature : cm1Score) {
            System.out.println(anFeature.toString());
        }
    }

    public void showCM2Scores() {
        System.out.print("\nCM2 Scores...\nFeature");
        for (int c = 0; c < numClasses; c++) {
            System.out.print("\t" + classLabels[c]);
        }
        System.out.println();
        for (CMScoreObject anFeature : cm2Score) {
            System.out.println(anFeature.toString());
        }
    }
/*
    public List<CMScoreObject> getTopNFeaturesCM1(int n) {
        List<CMScoreObject> topNScore = new ArrayList<CMScoreObject>();
        topNScore = getTopNFeatures(n, cm1Score);
        return topNScore;
    }

    public List<CMScoreObject> getTopNFeaturesCM2(int n) {
        List<CMScoreObject> topNScore = new ArrayList<CMScoreObject>();
        topNScore = getTopNFeatures(n, cm2Score);
        return topNScore;
    }

    public List<CMScoreObject> getTopNFeatures(int n, List<CMScoreObject> cmScore) {
        if (numClasses == 2) {
            return getTopNFeaturesBinaryClass(n, cmScore);
        } else {
            return getTopNFeaturesMultiClass(n, cmScore);
        }
    }

    
     public List<CMScoreObject> getTopNFeaturesMultiClass(int n, List<CMScoreObject> cmScore) {
        List<CMScoreObject> topScore = new ArrayList<CMScoreObject>();
        Collections.sort(cmScore);

        int topPos = 0;
        int mid = n / 2;
        if (n % 2 == 1) {
            topPos = mid + 1;
        } else {
            topPos = mid;
        }
        int startNeg = cmScore.size() - mid;

        for (int i = 0; i < topPos; i++) {
            topScore.add(cmScore.get(i));
        }

        for (int i = startNeg; i < cmScore.size(); i++) {
            topScore.add(cmScore.get(i));
        }
        return topScore;
    }
     
    public List<CMScoreObject> getTopNFeaturesBinaryClass(int n, List<CMScoreObject> cmScore) {
        List<CMScoreObject> topScore = new ArrayList<CMScoreObject>();
        Collections.sort(cmScore);

        int topPos = 0;
        int mid = n / 2;
        if (n % 2 == 1) {
            topPos = mid + 1;
        } else {
            topPos = mid;
        }
        int startNeg = cmScore.size() - mid;

        for (int i = 0; i < topPos; i++) {
            topScore.add(cmScore.get(i));
        }

        for (int i = startNeg; i < cmScore.size(); i++) {
            topScore.add(cmScore.get(i));
        }
        return topScore;
    }
    */
}
