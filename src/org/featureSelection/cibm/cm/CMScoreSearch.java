/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.featureSelection.cibm.cm;

import java.util.BitSet;
import java.util.Enumeration;
import java.util.Vector;
import weka.attributeSelection.ASEvaluation;
import weka.attributeSelection.ASSearch;
import weka.attributeSelection.UnsupervisedSubsetEvaluator;
import weka.core.Instances;
import weka.core.Option;
import weka.core.Utils;

/**
 *
 * @author mohammad
 */
public class CMScoreSearch extends ASSearch {

    /**
     * for serialization
     */
    static final long serialVersionUID = 5741842861142379712L;

    /**
     * the best feature set found during the search
     */
    private BitSet m_bestGroup;

    /**
     * does the data have a class
     */
    private boolean m_hasClass;

    /**
     * holds the class index
     */
    private int m_classIndex;

    /**
     * number of attributes in the data
     */
    private int m_numAttribs;
    private int m_topNAttribs;

    public String globalInfo() {
        return "CMScoreSearch : \n\nPerforms an bidirectional search through "
                + "the space of attribute. Reports the n length subset of attributes"
                + " with top n/2 positive and top n/2 negative CM scored attributes. ";
    }

    /**
     * Constructor
     */
    public CMScoreSearch() {
        //resetOptions();
    }

    /**
     * Returns an enumeration describing the available options.
     *
     * @return an enumeration of all the available options.
     *
     */
    public Enumeration listOptions() {
        Vector newVector = new Vector(1);

        newVector.addElement(new Option("\tOutput n number of top attributes."
                + "\n\t(default = all).", "N", 0, "-N"));
        return newVector.elements();
    }

    public void setOptions(String[] options)
            throws Exception {

        //resetOptions();

        setTopNAttributes(Integer.parseInt(Utils.getOption("N", options)));
    }

    public String TopNAttributesTipText() {
        return "Print progress information. Sends progress info to the terminal "
                + "as the search progresses.";
    }

    public void setTopNAttributes(int n) {
        m_topNAttribs = n;
    }

    public String[] getOptions() {
        String[] options = new String[1];
        int current = 0;

        if (m_topNAttribs > 0) {
            options[current++] = "-N";
        }

        while (current < options.length) {
            options[current++] = "";
        }
        return options;
    }

    /**
     * get whether or not output is verbose
     *
     * @return true if output is set to verbose
     */
    public int getTopNAttributes() {
        return m_topNAttribs;
    }

    @Override
    public int[] search(ASEvaluation ASEval, Instances data)
            throws Exception {
        int [] bestAttribs= new int [1];
        m_numAttribs = data.numAttributes();
        m_bestGroup = new BitSet(m_numAttribs);
        if (ASEval instanceof UnsupervisedSubsetEvaluator) {
            m_hasClass = false;
        } else {
            m_hasClass = true;
            m_classIndex = data.classIndex();
        }
        return bestAttribs;
    }

}
