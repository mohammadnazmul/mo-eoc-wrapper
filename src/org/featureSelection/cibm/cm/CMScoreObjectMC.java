/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.featureSelection.cibm.cm;

/**
 *
 * @author mohammad
 * Multi Class CM Score Object
 */
public class CMScoreObjectMC implements Comparable<CMScoreObjectMC>{
    String featureName;
    int featIdx;
    double [] cmScore;
    int clsIdx;
    
    CMScoreObjectMC(String featName, int index, int cls, double [] score)
    {
        this.featureName = featName;
        this.featIdx = index;
        this.cmScore = score;
        this.clsIdx = cls;
    }
    
    
    public String getFeatName()
    {
        return this.featureName;
    }
    public int getFeatIndex()
    {
        return this.featIdx;
    }
    public int getNumClasses()
    {
        return this.cmScore.length;
    }
    
    public double getCMScore(int clsPos)
    {
        return this.cmScore[clsPos];
    }
    
    public double [] getCMScores()
    {
        return this.cmScore;
    }
    
    
    @Override
    public String toString()
    {
        String txt = this.featureName;
        for(int c=0; c< this.cmScore.length; c++)
            txt += ("\t"+cmScore[c]);
        return (txt);
    }

    @Override
    public int compareTo(CMScoreObjectMC another) {
        if (this.getCMScore(clsIdx)>another.getCMScore(clsIdx)){
            return -1;
        }else{
            return 1;
        }
    }
}
