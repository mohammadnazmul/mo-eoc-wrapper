/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.featureSelection.cibm.cm;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.attributeSelection.ASEvaluation;
import static weka.attributeSelection.ASEvaluation.runEvaluator;
import weka.attributeSelection.AttributeEvaluator;
import weka.core.Capabilities;
import weka.core.Instances;
import weka.core.Option;
import weka.core.OptionHandler;
import weka.core.RevisionUtils;
import weka.core.Utils;

/**
 *
 * @author mohammad
 */
public class CMScoreAttributeEval extends ASEvaluation implements
        AttributeEvaluator, OptionHandler {

    /**
     * for serialization
     */
    static final long serialVersionUID = -8504656625598579926L;

    /**
     * The training instances
     */
    private Instances m_trainInstances;

    /**
     * The class index
     */
    //private int m_classIndex;
    /**
     * The number of attributes
     */
    //private int m_numAttribs;
    private int m_topNfeature;

    /**
     * Merge missing values
     */
    private boolean m_show_feature_name;
    private int m_cm_score_no;
    private int m_multi_class_idx;

    private String m_cm_score_name;
    private boolean isDebug = false;

    private List<CMScoreObject> cmFeatures;

    /**
     * Returns a string describing this attribute evaluator
     *
     * @return a description of the evaluator suitable for displaying in the
     * explorer/experimenter gui
     */
    public String globalInfo() {
        return "CMScoreAttributeEval :\n\nEvaluates the worth of an attribute (f)"
                + "by measuring the CM Scores (both of the CM1 and CM2).\n\n"
                + "CM_1(f,X,Y)=(μ^f_X - μ^f_Y)/(1 + max(y_f) - min(y_f))\n"
                + "CM_2(f,X,Y)=(μ^f_X - μ^f_Y)/(1 + min(max(y_f) - min(y_f),max(x_f) - min(x_f)).\n";
    }

    /**
     * Constructor
     */
    public CMScoreAttributeEval() {
        resetOptions();
    }

    /**
     * Returns an enumeration describing the available options.
     *
     * @return an enumeration of all the available options.
     *
     */
    @Override
    public Enumeration listOptions() {
        Vector newVector = new Vector(3);
        newVector.addElement(new Option("\tValue of CM Score (CM_1 or CM_2) wants to evaluate.", "C", 1, "-C"));
        newVector.addElement(new Option("\tClass Index for Multi Class.", "i", 0, "-i"));
        newVector.addElement(new Option("\tHow many attributes to select?", "N", 0, "-N"));
        return newVector.elements();
    }

    @Override
    public void setOptions(String[] options) {
        try {
            resetOptions();
            String str = Utils.getOption("i", options);
            if (str.isEmpty() != true) {
                int clsId = Integer.parseInt(str);
                setMultiClassIndex(clsId);
            }
            str = Utils.getOption("N", options);
            if (str.isEmpty() != true) {
                int nfeat = Integer.parseInt(str);
                setTopNFeat(nfeat);
            }
            str = Utils.getOption("C", options);
            if (str.isEmpty() != true) {
                int cm = Integer.parseInt(str);
                setWhichCM(cm);
            }

        } catch (Exception ex) {
            Logger.getLogger(CMScoreAttributeEval.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String showTopNFeatTipText() {
        return "How many attributes to Select.";
    }

    public void setTopNFeat(int b) {
        m_topNfeature = b;
    }

    public int getTopNFeat() {
        return m_topNfeature;
    }

    public String showFeatureNamesTipText() {
        return "Enable to show only the names of selected attributes. "
                + "Otherwise, all attribute names with thier scores will be shown.";
    }

    /**
     * show only feature names or show feature name with their scores
     *
     * @param b true=show feature names.
     */
    public void setShowFeatureNames(boolean b) {
        m_show_feature_name = b;
    }

    public boolean getShowFeatureNames() {
        return m_show_feature_name;
    }

    public String WhichCMTipText() {
        return "Enable to show only the names of selected attributes. "
                + "Otherwise, all attribute names with thier scores will be shown.";
    }

    public void setWhichCM(int b) {
        m_cm_score_no = b;
        if (b == 1) {
            m_cm_score_name = "CM1";
        } else {
            m_cm_score_name = "CM2";
        }
    }

    /**
     * get whether show only feature names or show feature name with their
     * scores
     *
     * @return true if missing values are being distributed.
     */
    public int getWhichCM() {
        return m_cm_score_no;
    }

    public String getWhichCMName() {
        return m_cm_score_name;
    }

    public void setMultiClassIndex(int ci) {
        m_multi_class_idx = ci;

    }

    /**
     * get whether show only feature names or show feature name with their
     * scores
     *
     * @return true if missing values are being distributed.
     */
    public int getMultiClassIndex() {
        return m_multi_class_idx;
    }

    /**
     * Gets the current settings of WrapperSubsetEval.
     *
     * @return an array of strings suitable for passing to setOptions()
     */
    @Override
    public String[] getOptions() {
        String[] options = new String[1];
        int current = 0;

        if (!getShowFeatureNames()) {
            options[current++] = "-F";
        }

        while (current < options.length) {
            options[current++] = "";
        }

        return options;
    }

    /**
     * Returns the capabilities of this evaluator.
     *
     * @return the capabilities of this evaluator
     * @see Capabilities
     */
    @Override
    public Capabilities getCapabilities() {
        Capabilities result = super.getCapabilities();
        result.disableAll();

        // attributes
        result.enable(Capabilities.Capability.NUMERIC_ATTRIBUTES);

        // class
        result.enable(Capabilities.Capability.NOMINAL_CLASS);

        return result;
    }

    /**
     * Initializes a gain ratio attribute evaluator. Discretizes all attributes
     * that are numeric.
     *
     * @param data set of instances serving as training data
     * @throws Exception if the evaluator has not been generated successfully
     */
    @Override
    public void buildEvaluator(Instances data) throws Exception {

        // can evaluator handle data?
        getCapabilities().testWithFail(data);

        m_trainInstances = data;
//        m_classIndex = m_trainInstances.classIndex();
//        m_numAttribs = m_trainInstances.numAttributes();
//        m_numInstances = m_trainInstances.numInstances();

        // Step 2 : Instantiate CMScoreCalculator Calculator Object
        CMScoreCalculator CM = new CMScoreCalculator(m_trainInstances, isDebug);

        // Step 3 : Use CM1 Calculator
        cmFeatures = new ArrayList<CMScoreObject>();

        if (getWhichCM() == 1) {
            CM.CalculateCM1();
            cmFeatures = CM.getCM1ScoreObject();
        } else {
            CM.CalculateCM2();
            cmFeatures = CM.getCM2ScoreObject();
        }
    }

    /**
     * reset options to default values
     */
    protected void resetOptions() {
        m_trainInstances = null;
        m_show_feature_name = false;
        m_multi_class_idx = -1;
    }

    /**
     * evaluates an individual attribute by measuring the CM Score of given the
     * attribute.
     *
     * @param attribute the index of the attribute to be evaluated
     * @return the CM score
     * @throws Exception if the attribute could not be evaluated
     */
    @Override
    public double evaluateAttribute(int attribute) throws Exception {
        if (getMultiClassIndex() == -1) {
            return cmFeatures.get(attribute).getCMScore(0);
        } else {
            return cmFeatures.get(attribute).getCMScore(m_multi_class_idx);
        }
    }

    /**
     * Return a description of the evaluator
     *
     * @return description as a string
     */
    @Override
    public String toString() {
        StringBuffer text = new StringBuffer();

        if (m_trainInstances == null) {
            text.append("\tCM_" + m_cm_score_no + " Score evaluator has not been built");
        } else {
            text.append("\tCM_" + m_cm_score_no + " Score feature evaluator");
        }

        text.append("\n");
        return text.toString();
    }

    /**
     * Returns the revision string.
     *
     * @return the revision
     */
    @Override
    public String getRevision() {
        return RevisionUtils.extract("$Revision: 1 $");
    }

    /**
     * Main method.
     *
     * @param args the options -t training file
     */
    public static void main(String[] args) {
        runEvaluator(new CMScoreAttributeEval(), args);
    }
}
