/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.featureSelection.weka;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.functionlib.wekaFunctions.retainFeatures;
import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.CorrelationAttributeEval;
import weka.attributeSelection.GainRatioAttributeEval;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.attributeSelection.SymmetricalUncertAttributeEval;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/**
 *
 * @author mohammad 1) Instantiate an object objFS = new
 * FeatureSelection(Instances arffData); 2) call generateRanks(); 3) Instances
 * data = getArffWithTopFeatures(int fsMethod, double percnt);
 */
public class FeatureSelection {

    public boolean isDebug = true;
    Instances inputData;
    String dataPath;
    int numberOfFeatures;
    int numberOfClasses;
    int clsIdx;
    String relName;
    // holds rank of features
    /**
     * the attribute indexes and associated merits if a ranking is produced
     */
    double[][] ranksIG;
    double[][] ranksGR;
    double[][] ranksCR;
    double[][] ranksSU;

    public FeatureSelection(Instances arffData) {
        inputData = arffData;
        numberOfFeatures = inputData.numAttributes() - 1;
        numberOfClasses = inputData.numClasses();
        clsIdx = inputData.classIndex()+1;
        relName=arffData.relationName();
    }

    public void generateRanks() {
        String nRank = "" + numberOfFeatures;
        String[] options = {"-T", "-1.7976931348623157E308", "-N", nRank};

        FSInfoGain(inputData, options);
        FSGainRatio(inputData, options);
        FSCorrelation(inputData, options);
        FSSymmetricalUncert(inputData, options);
    }

    public Instances getArffWithTopFeatures(int fsMethod, double percnt) throws Exception {
        int numberOfSelFeatures = (int) (numberOfFeatures * percnt);
        Instances selFeatureData = getNFeatures(fsMethod, numberOfSelFeatures);
        selFeatureData.setRelationName(relName);
        return selFeatureData;
    }

    public Instances getArffWithTopFeatures(int fsMethod, int nFeature) throws Exception {
        Instances selFeatureData = getNFeatures(fsMethod, nFeature);
        selFeatureData.setRelationName(relName);
        return selFeatureData;
    }

    Instances getNFeatures(int fsMethod, int nFeature) throws Exception {
        Instances selFeatureData = null;
        String indices = "";

        switch (fsMethod) {
            case 2:                // strFSName = "IG";
                for (int i = 0; i < nFeature; i++) {
                    int idx = ((int) ranksIG[i][0] + 1);
                    if (idx != clsIdx) {
                        indices += (idx + ",");
                    }
                }
                indices += clsIdx;
                selFeatureData = retainFeatures(inputData, indices);

                break;
            case 3:                // strFSName = "GR";
                for (int i = 0; i < nFeature; i++) {
                    int idx = ((int) ranksGR[i][0] + 1);
                    if (idx != clsIdx) {
                        indices += (idx + ",");
                    }
                }
                indices += clsIdx;
                selFeatureData = retainFeatures(inputData, indices);

                break;
            case 4:                // strFSName = "CR";
                for (int i = 0; i < nFeature; i++) {
                    int idx = ((int) ranksCR[i][0] + 1);
                    if (idx != clsIdx) {
                        indices += (idx + ",");
                    }
                }
                indices += clsIdx;
                selFeatureData = retainFeatures(inputData, indices);

                break;
            case 5:                // strFSName = "SU";
                for (int i = 0; i < nFeature; i++) {
                    int idx = ((int) ranksSU[i][0] + 1);
                    if (idx != clsIdx) {
                        indices += (idx + ",");
                    }
                }
                indices += clsIdx;
                selFeatureData = retainFeatures(inputData, indices);
                break;
        }
        selFeatureData.setRelationName(relName);
        
        return selFeatureData;
    }

   
    protected void FSInfoGain(Instances data, String[] rankSearchOptions) {

        AttributeSelection attSelection = new AttributeSelection();

        // Step 1: setEvaluator
        InfoGainAttributeEval eval = new InfoGainAttributeEval();
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        try {
            rank.setOptions(rankSearchOptions);
        } catch (Exception ex) {
            System.err.println(FeatureSelection.class.getName() + " : Information Gain Evaluation Search Error");
        }
        attSelection.setSearch(rank);

        try {
            // Step 3: SelectAttributes
            attSelection.SelectAttributes(data);
        } catch (Exception ex) {

            System.err.println(FeatureSelection.class.getName() + " : Unable to select attribute");
            //Logger.getLogger(FeatureSelection.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        ranksIG = new double[numberOfFeatures][2];
        try {
            ranksIG = attSelection.rankedAttributes();
        } catch (Exception ex) {
            System.err.println(FeatureSelection.class.getName() + " : Unable to Rank Features");

        }
    }

    protected void FSGainRatio(Instances data, String[] rankSearchOptions) {

        AttributeSelection attSelection = new AttributeSelection();

        // Step 1: setEvaluator
        GainRatioAttributeEval eval = new GainRatioAttributeEval();
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        try {
            rank.setOptions(rankSearchOptions);
        } catch (Exception ex) {
            System.err.println(FeatureSelection.class.getName() + " : Information Gain Evaluation Search Error");
        }
        attSelection.setSearch(rank);

        try {
            // Step 3: SelectAttributes
            attSelection.SelectAttributes(data);
        } catch (Exception ex) {
            System.err.println(FeatureSelection.class.getName() + " : Unable to select attribute");
        }
        ranksGR = new double[numberOfFeatures][2];
        try {
            ranksGR = attSelection.rankedAttributes();
        } catch (Exception ex) {
            System.err.println(FeatureSelection.class.getName() + " : Unable to select attribute");

        }
    }

    protected void FSCorrelation(Instances data, String[] rankSearchOptions) {

        AttributeSelection attSelection = new AttributeSelection();

        // Step 1: setEvaluator
        CorrelationAttributeEval eval = new CorrelationAttributeEval();
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        try {
            rank.setOptions(rankSearchOptions);
        } catch (Exception ex) {
            System.err.println(FeatureSelection.class.getName() + " : Coorelation Evaluation Search Error");
        }
        attSelection.setSearch(rank);

        try {
            // Step 3: SelectAttributes
            attSelection.SelectAttributes(data);
        } catch (Exception ex) {
            System.err.println(FeatureSelection.class.getName() + " : Unable to select attribute");
        }
        ranksCR = new double[numberOfFeatures][2];
        try {
            ranksCR = attSelection.rankedAttributes();
        } catch (Exception ex) {
            System.err.println(FeatureSelection.class.getName() + " : Unable to select attribute");
        }
    }

    protected void FSSymmetricalUncert(Instances data, String[] rankSearchOptions) {

        AttributeSelection attSelection = new AttributeSelection();

        // Step 1: setEvaluator
        SymmetricalUncertAttributeEval eval = new SymmetricalUncertAttributeEval();
        attSelection.setEvaluator(eval);

        //Step 2: setSearch
        Ranker rank = new Ranker();
        try {
            rank.setOptions(rankSearchOptions);
        } catch (Exception ex) {
            System.err.println(FeatureSelection.class.getName() + " : Symmetric Uncertainity Evaluation Search Error");
        }
        attSelection.setSearch(rank);

        try {
            // Step 3: SelectAttributes
            attSelection.SelectAttributes(data);
        } catch (Exception ex) {
            System.err.println(FeatureSelection.class.getName() + " : Unable to select attribute");
        }
        ranksSU = new double[numberOfFeatures][2];
        try {
            ranksSU = attSelection.rankedAttributes();
        } catch (Exception ex) {
            System.err.println(FeatureSelection.class.getName() + " : Unable to select attribute");
        }
    }
}
