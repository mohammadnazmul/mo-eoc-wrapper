/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.functionlib;

/**
 *
 * @author mohammad
 */
public class ensembleFunctions {

    public static int numOfClassifiers(String ensmCmb) {
        int numOfClsf = 0;
        for (int i = 0; i < ensmCmb.length(); i++) {
            if (ensmCmb.charAt(i) == '1') {
                numOfClsf++;
            }
        }
        return numOfClsf;
    }

    public static String getFSName(int intFS) {
        String strFSName = "";
        switch (intFS) {
            case 0:
                strFSName = "CM1";
                break;
            case 1:
                strFSName = "CM2";
                break;
            case 2:
                strFSName = "IG";
                break;
            case 3:
                strFSName = "GR";
                break;
            case 4:
                strFSName = "CR";
                break;
            case 5:
                strFSName = "SU";
                break;
        }
        return strFSName;
    }


}
