/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.functionlib;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import static mo.eoc.wrapper.MainProgram.cmFS;
import static mo.eoc.wrapper.MainProgram.fsLen;
import static mo.eoc.wrapper.MainProgram.pctLen;
import static mo.eoc.wrapper.MainProgram.wekaFS;
import weka.attributeSelection.ASEvaluation;
import weka.attributeSelection.ASSearch;
import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.GreedyStepwise;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.misc.InputMappedClassifier;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.ReplaceMissingValues;

/**
 *
 * @author mohammad
 */
public class wekaFunctions {

    /**
     * loads the given ARFF file and sets the class attribute as the last
     * attribute.
     *
     * @param filename the file to load
     * @return loaded Arff instances
     */
    public static Instances loadArff(String filename) {
        ConverterUtils.DataSource source = null;
        Instances trnData = null;
        // Load Training data        
        try {
            // TODO code application logic here
            source = new ConverterUtils.DataSource(filename);
        } catch (Exception ex) {
            System.err.println("Error::: Unable to read " + filename);
            Logger.getLogger(wekaFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            trnData = source.getDataSet();
        } catch (Exception ex) {
            System.err.println("Error::: Unable to get data from " + filename);
            Logger.getLogger(wekaFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (trnData.classIndex() < 0) {
            trnData.setClassIndex(trnData.numAttributes() - 1);
        }
        return trnData;
    }

    public static Instances loadArff(String filename, int clsIdx) {
        Instances trnData = loadArff(filename);
        trnData.setClassIndex(clsIdx);
        return trnData;
    }

    /**
     * saves the data to the specified file
     *
     * @param data the data to save to a file
     * @param filename the file to save the data to
     */
    public static void saveArff(Instances data, String filename) throws IOException {
        //try {
        ArffSaver saver = new ArffSaver();
        saver.setInstances(data);
        saver.setFile(new File(filename));
        saver.writeBatch();
        //} catch (IOException ex) {
        //    System.err.println("Error::: Unable to Write " + filename);
        //    Logger.getLogger(wekaFunctions.class.getName()).log(Level.SEVERE, null, ex);
        // }
    }

    /**
     * do data filtering to reduce dimensionality
     *
     * @param data the data to save to a file
     * @param code the code to execute the Ranker Feature Selection
     * @throws Exception if something goes wrong
     * @return reduced arff instance
     */
    public static Instances reduceDimensionality(Instances data) throws Exception {
        Instances reducData = CFEval(data);
        return reducData;
    }

    protected static Instances useFilter(Instances data, ASEvaluation eval, ASSearch search) throws Exception {
        weka.filters.supervised.attribute.AttributeSelection filter = new weka.filters.supervised.attribute.AttributeSelection();
        filter.setEvaluator(eval);
        filter.setSearch(search);
        filter.setInputFormat(data);
        Instances newData = Filter.useFilter(data, filter);
        //System.out.println("#selected attributes: " + newData.numAttributes());
        return newData;

    }

    protected static Instances CFEval(Instances data) throws Exception {
        Instances newData = data;

        CfsSubsetEval eval = new CfsSubsetEval();
        GreedyStepwise search = new GreedyStepwise();
        search.setSearchBackwards(true);

        // generate new data
        newData = useFilter(data, eval, search);
        //System.out.println("#Selected Attributes: " + newData.numAttributes());
        return newData;
    }

    public static Instances retainFeatures(Instances data, String indices) throws Exception {
        Instances selFeatureData = null;

        Remove remove = new Remove();
        remove.setAttributeIndices(indices);
        remove.setInvertSelection(true);
        remove.setInputFormat(data);
        selFeatureData = Filter.useFilter(data, remove);
        int cid = selFeatureData.numAttributes() - 1;
        selFeatureData.setClassIndex(cid);

        return selFeatureData;
    }

    public static Instances replaceMissing(Instances data) throws Exception {
        ReplaceMissingValues replaceMissingFilter = new ReplaceMissingValues();
        replaceMissingFilter.setInputFormat(data);
        Instances rplcdInstances = Filter.useFilter(data, replaceMissingFilter);
        return rplcdInstances;
    }

    public static Instances trainTestCompatible(Instances trn, Instances tst) throws Exception {
        String featIdx = "";
        String relName = trn.relationName();

        int nfeat = trn.numAttributes() - 1;
        for (int i = 0; i < nfeat; i++) {
            String featName = trn.attribute(i).name();
            int idx = tst.attribute(featName).index() + 1;
            featIdx += (idx + ",");
        }
        featIdx += (tst.classIndex() + 1);

        Instances instNew = retainFeatures(tst, featIdx);

        instNew.setRelationName(relName);
        //instNew.setClassIndex(instNew.numAttributes() - 1);
        return instNew;
    }

    public static String evaluateBaseClassifiers(Classifier[] baseClassifiers, Instances trn, Instances tst) throws Exception {
        String strOut = "\n\n==================================================================\n"
                + "Base Classifier\tMCC\tAcc\tPrec\tF-Measure\tAUC"
                + "\n==================================================================\n";

        for (int i = 0; i < baseClassifiers.length; i++) {
            // build and store the model into ensemble
            Classifier clsCopy = AbstractClassifier.makeCopy(baseClassifiers[i]);
            clsCopy.buildClassifier(trn);
            String clsName = clsCopy.getClass().getSimpleName();
            // evaluate EOC and get mcc
            InputMappedClassifier inpMapper = new InputMappedClassifier();
            String mapperArgs[] = {"-I", "-trim", "-M"};

            inpMapper.setOptions(mapperArgs);
            inpMapper.setClassifier(clsCopy);
            inpMapper.buildClassifier(trn);
            Evaluation eval = new Evaluation(trn);
            eval.evaluateModel(inpMapper, tst);
            double mcc = (eval.weightedMatthewsCorrelation());
            double acc = (eval.pctCorrect());
            double prec = eval.weightedPrecision();
            double fm = eval.weightedFMeasure();
            double auc = (eval.weightedAreaUnderROC());
            strOut += (clsName + "\t" + mcc + "\t" + acc + "\t" + prec + "\t" + fm + "\t" + auc + "\n");
        }
        return strOut;
    }
    
    public static String evaluateWekaEoC(Classifier[] ensembleClassifiers, Instances trn, Instances tst) throws Exception {
        String strOut = "\n\n==================================================================\n"
                + "Ensemble Classifier\tMCC\tAcc\tPrec\tF-Measure\tAUC"
                + "\n==================================================================\n";

        for (int i = 0; i < ensembleClassifiers.length; i++) {
            // build and store the model into ensemble
            Classifier clsCopy = AbstractClassifier.makeCopy(ensembleClassifiers[i]);
            clsCopy.buildClassifier(trn);
            String clsName = clsCopy.getClass().getSimpleName();
            // evaluate EOC and get mcc
            InputMappedClassifier inpMapper = new InputMappedClassifier();
            String mapperArgs[] = {"-I", "-trim", "-M"};

            inpMapper.setOptions(mapperArgs);
            inpMapper.setClassifier(clsCopy);
            inpMapper.buildClassifier(trn);
            Evaluation eval = new Evaluation(trn);
            eval.evaluateModel(inpMapper, tst);
            double mcc = (eval.weightedMatthewsCorrelation());
            double acc = (eval.pctCorrect());
            double prec = eval.weightedPrecision();
            double fm = eval.weightedFMeasure();
            double auc = (eval.weightedAreaUnderROC());
            strOut += (clsName + "\t" + mcc + "\t" + acc + "\t" + prec + "\t" + fm + "\t" + auc + "\n");
        }
        return strOut;
    }
    
    public static String tenFoldBaseClassifiers(Classifier[] baseClassifiers, Instances trn) throws Exception {
        String strOut = "\n\n==================================================================\n"
                + "Base Classifier\tMCC\tAcc\tPrec\tF-Measure\tAUC"
                + "\n==================================================================\n";

        for (int i = 0; i < baseClassifiers.length; i++) {
            // build and store the model into ensemble
            Classifier clsCopy = AbstractClassifier.makeCopy(baseClassifiers[i]);
            
            String clsName = clsCopy.getClass().getSimpleName();
            // evaluate EOC and get mcc
            Evaluation eval = new Evaluation(trn);
            eval.crossValidateModel(clsCopy, trn, 10, new Random(1));
            double mcc = (eval.weightedMatthewsCorrelation());
            double acc = (eval.pctCorrect());
            double prec = eval.weightedPrecision();
            double fm = eval.weightedFMeasure();
            double auc = (eval.weightedAreaUnderROC());
            strOut += (clsName + "\t" + mcc + "\t" + acc + "\t" + prec + "\t" + fm + "\t" + auc + "\n");
        }
        return strOut;
    }
    
    public static String tenFoldWekaEoC(Classifier[] ensembleClassifiers, Instances trn) throws Exception {
        String strOut = "\n\n======================================================================\n"
                + "Ensemble Classifier\tMCC\tAcc\tPrec\tF-Measure\tAUC"
                + "\n======================================================================\n";

        for (int i = 0; i < ensembleClassifiers.length; i++) {
            // build and store the model into ensemble
            Classifier clsCopy = AbstractClassifier.makeCopy(ensembleClassifiers[i]);
            
            String clsName = clsCopy.getClass().getSimpleName();
            // evaluate EOC and get mcc
            Evaluation eval = new Evaluation(trn);
            eval.crossValidateModel(clsCopy, trn, 10, new Random(1));
            double mcc = (eval.weightedMatthewsCorrelation());
            double acc = (eval.pctCorrect());
            double prec = eval.weightedPrecision();
            double fm = eval.weightedFMeasure();
            double auc = (eval.weightedAreaUnderROC());
            strOut += (clsName + "\t" + mcc + "\t" + acc + "\t" + prec + "\t" + fm + "\t" + auc + "\n");
        }
        return strOut;
    }

    public static Instances getTopNFeatureData(int fsNo, int nfeat) throws Exception {
        Instances trnPctArffData = null;
        if (fsNo > 1) {
            trnPctArffData = wekaFS.getArffWithTopFeatures(fsNo, nfeat);
        } else {
            trnPctArffData = cmFS.getArffWithTopFeatures(fsNo, nfeat);
        }
        return trnPctArffData;
    }

    public static Instances getTopNFeatureData(String strFSInfo, int nFeatures) {
        Instances trnPctArffData = null;
        String strFS = strFSInfo.substring(0, fsLen);
        String strPct = strFSInfo.substring(fsLen, fsLen + pctLen);

        int intPct = Integer.parseInt(strPct, 2);
        int numberOfSelFeatures = nFeatures;

        // GETTING NUM OF FEATURES TO BE SELECTED
        if ((nFeatures < 100)) {
            numberOfSelFeatures = intPct;
        } else {
            double dblPercnt = (intPct / 100.0);
            numberOfSelFeatures = (int) (nFeatures * dblPercnt);
        }

        int fsNo = Integer.parseInt(strFS, 2);
        try {
            if (fsNo > 1) {
                trnPctArffData = wekaFS.getArffWithTopFeatures(fsNo, numberOfSelFeatures);
            } else {
                trnPctArffData = cmFS.getArffWithTopFeatures(fsNo, numberOfSelFeatures);
            }
        } catch (Exception ex) {
            System.err.println("Error Creating Solution Arff.");
        }
        return trnPctArffData;
    }
}
