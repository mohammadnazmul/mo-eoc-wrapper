/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eocframework.problem.eoc;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import static mo.eoc.wrapper.MainProgram.fsLen;
import static mo.eoc.wrapper.MainProgram.globalDebug;
import static mo.eoc.wrapper.MainProgram.pctLen;
import static org.functionlib.ensembleFunctions.numOfClassifiers;
import static org.functionlib.wekaFunctions.getTopNFeatureData;
import org.eocframework.problem.eval.EvaluateMOEnsembleObjs;
import org.moeaframework.core.Problem;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.BinaryVariable;
import org.moeaframework.core.variable.EncodingUtils;
import org.moeaframework.problem.AbstractProblem;
import weka.core.Instances;

/**
 *
 * @author c3172331
 */
public class ObjMCCDiversity extends AbstractProblem implements Problem, Serializable {

    private static final long serialVersionUID = 1125899839733759L;  // one of Carol prime number
    static boolean isMultiClass;
    private final int numberOfBits;
    private final int nFeatures;
    //private Instances trnPctArffData;
    //private boolean isEoCinit = false;
    private static int kFold;
    private static boolean isParallel;

    //private EvaluateMOEnsembleObjs eocObject;
    // parts of an individual
    String strIndiv;
    String strCmb;
    String strStart;
    int numberOfSelFeatures;
    int intFS;
    int intPct;

    /**
     *
     */
    public ObjMCCDiversity() {
        super(1, 2,1);
        this.numberOfBits=29;
        nFeatures = 100;
    }
    
    public ObjMCCDiversity(int numberOfClassifiers, int nfeat) {
        super(1, 2,1);
        this.numberOfBits = numberOfClassifiers;
        nFeatures = nfeat;
    }

    public ObjMCCDiversity(int numberOfClassifiers, int nfeat, boolean ismulti, int kf) {
        super(1, 2,1);
        this.numberOfBits = numberOfClassifiers;
        nFeatures = nfeat;
        isMultiClass = ismulti;
        kFold = kf;
    }
    
    public ObjMCCDiversity(int numberOfClassifiers, int nfeat, boolean ismulti, int kf, boolean isParl) {
        super(1, 2,1);
        this.numberOfBits = numberOfClassifiers;
        nFeatures = nfeat;
        isMultiClass = ismulti;
        kFold = kf;
        isParallel = isParl;
    }

    @Override
    public String getName() {
        return "MO-EoC : Objectives (Diversity, MCC)";
    }

    @Override
    public int getNumberOfVariables() {
        return 1;
    }

    @Override
    public int getNumberOfObjectives() {
        return 2;
    }

    @Override
    public int getNumberOfConstraints() {
        return 1;
    }

    @Override
    public void evaluate(Solution solution) {
        // Getting values of variables
        strIndiv = solution.getVariable(0).toString();

        // Strip out :: FS-Bits pct-Bits cmb-Bits
        String strFS = strIndiv.substring(0, fsLen);
        String strPct = strIndiv.substring(fsLen, fsLen + pctLen);
        strStart = strFS + strPct;

        strCmb = strIndiv.substring(fsLen + pctLen);
        if (numOfClassifiers(strCmb) < 1) {
            if (globalDebug == true) {
                System.err.println("Invalid Soln: " + strIndiv);
            }
            solution.setObjective(0, 1.0);
            solution.setObjective(1, 1.0);
            solution.setConstraint(0, 1.0);
        } else {
            // Parse values for FS method and Percentage
            intFS = Integer.parseInt(strFS, 2);
            intPct = Integer.parseInt(strPct, 2);

            // GETTING NUM OF FEATURES TO BE SELECTED
            if ((nFeatures < 100)) {
                numberOfSelFeatures = intPct;
            } else {
                double dblPercnt = (intPct / 100.0);
                numberOfSelFeatures = (int) (nFeatures * dblPercnt);
            }

            // DISCARD invalid solution by providing mcc=-1.0 and diversity=0.0
            double dblDivrScore = 0.0;
            double dblMccScore = -1.0;
            double constrs = dblMccScore - 0.0000001;

            // if FS method is invalid or percentage is GT half of features OR has at leat one feature + class attribute
            if (intFS > 5 || (numberOfSelFeatures >= nFeatures / 2) || numberOfSelFeatures < 2) {
                if (globalDebug == true) {
                    System.err.println("Reject Soln: " + strIndiv);
                }
                solution.setObjective(0, 1.0);
                solution.setObjective(1, 1.0);
                solution.setConstraint(0, 1.0);
            } else {
                // Select top N number of Features for the FS method
                Instances trainData = null;
                try {
                    trainData = getTopNFeatureData(intFS, numberOfSelFeatures);
                } catch (Exception ex) {
                    System.err.println("Error selecting features for: " + strIndiv + "\t" + intFS + "\tTop " + numberOfSelFeatures);
                    Logger.getLogger(ObjMCCDiversity.class.getName()).log(Level.SEVERE, null, ex);
                }

                EvaluateMOEnsembleObjs eocObject = new EvaluateMOEnsembleObjs(strCmb, strStart, isMultiClass, isParallel);
                // evaluating values of objectives      
                if (kFold <= 2) {
                    //System.out.println("Split..");
                    dblMccScore = eocObject.getSplitFitnessMCC(trainData);
                    dblDivrScore = eocObject.getSplitDiversityScore();
                } else { // do k-Fold cross validation
                    //System.out.println(kFold + "-fold CV..");
                    dblMccScore = eocObject.getCVFitnessMCC(trainData, kFold);
                    dblDivrScore = eocObject.getCVDiversityScore();
                }
                //Now calculate the constraint violations.
                //The constraints are greater than or equal to constraint on obj
                if (dblMccScore > 0.0) {
                    constrs = 0.0;
                } else {
                    constrs = dblMccScore - 0.0000001;
                }
            }
            // Setting objectives values (- for maximisation)
            solution.setObjective(0, -dblMccScore);
            solution.setObjective(1, -dblDivrScore);
            solution.setConstraint(0, constrs);

            if (globalDebug == true) {
                System.out.println("Evaluated Soln: " + strIndiv + "\t" + dblMccScore + "\t" + dblDivrScore);
            }
        }
    }

    public Solution setSolution(String cmb) {
        Solution solution = new Solution(1, 2, 1);
        int solLen = cmb.length();
        BinaryVariable var = EncodingUtils.newBinary(solLen);
        for (int idx = 0; idx < solLen; idx++) {
            if (cmb.charAt(idx) == '1') {
                var.set(idx, true);
            } else {
                var.set(idx, false);
            }
        }
        solution.setVariable(0, var);
        return solution;
    }

    @Override
    public Solution newSolution() {
        Solution solution = new Solution(1, 2, 1);
        int solLen = fsLen + pctLen + numberOfBits;

        solution.setVariable(0, EncodingUtils.newBinary(solLen));     // base classifier choice holder

        return solution;
    }

    @Override
    public void close() {
        // do nothing
    }

    @Override
    public String toString() {
        String strDisp = "";

        return strDisp;
    }
}
