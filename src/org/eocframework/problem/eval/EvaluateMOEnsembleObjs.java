package org.eocframework.problem.eval;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.MLPClassifier;
import weka.classifiers.functions.RBFNetwork;
import weka.classifiers.functions.SGD;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.SPegasos;
import weka.classifiers.functions.SimpleLogistic;
import weka.classifiers.functions.VotedPerceptron;
import weka.classifiers.lazy.IBk;
import weka.classifiers.misc.VFI;
import weka.classifiers.rules.ConjunctiveRule;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.rules.JRip;
import weka.classifiers.rules.PART;
import weka.classifiers.rules.Ridor;
import weka.classifiers.trees.ADTree;
import weka.classifiers.trees.BFTree;
import weka.classifiers.trees.DecisionStump;
import weka.classifiers.trees.ExtraTree;
import weka.classifiers.trees.FT;
import weka.classifiers.trees.HoeffdingTree;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.LADTree;
import weka.classifiers.trees.LMT;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.RandomTree;
import weka.classifiers.trees.SimpleCart;
import weka.core.Instances;

import static mo.eoc.wrapper.MainProgram.globalDebug;
import weka.classifiers.misc.InputMappedClassifier;

/**
 *
 * @author mohammad
 */
public class EvaluateMOEnsembleObjs implements Serializable {

    static final long serialVersionUID = 1125899839733759L;

    int ensmSize;
    int mSampTest;

    String strCmb;
    String strFsPct;

    /*
     Instances trainFullData;
     Instances spltTest;

     Instances evalTrainData;
     Instances evalTestData;

    
     Evaluation eval;
     */
    boolean isSplitEvalDone = false;
    boolean isCVEvalDone = false;
    boolean isParallel = false;

    double splitMCC = -2.0;
    double cvMCC = -2.0;

    double splitDivrScore = 0.0;
    double cvDivrScore = 0.0;

    //boolean isModelBuild = false;
    boolean isMultiClass = false;
    boolean isvalidFull = false;

    double[][] ensmPredictions;

    public static Classifier[] baseClassifiers;

    public static Classifier[] baseClassifiersMultiClass = {
        new BayesNet(), new NaiveBayes(),
        new SMO(),
        new Logistic(), new SimpleLogistic(),
        new MLPClassifier(), new RBFNetwork(),
        new BFTree(), new HoeffdingTree(), new J48(), new LADTree(), new REPTree(), new PART(),
        new DecisionStump(),
        new ExtraTree(), new FT(), new LMT(), new RandomTree(), new SimpleCart(),
        new IBk(),
        new ConjunctiveRule(), new JRip(), new Ridor(),
        new DecisionTable(),
        new VFI()};

    public static Classifier[] baseClassifiersBinaryClass = {
        new BayesNet(), new NaiveBayes(),
        new SMO(), new SPegasos(),
        new Logistic(), new SimpleLogistic(), new SGD(),
        new MLPClassifier(), new RBFNetwork(), new VotedPerceptron(),
        new ADTree(), new BFTree(), new HoeffdingTree(), new J48(), new LADTree(), new REPTree(), new PART(),
        new DecisionStump(),
        new ExtraTree(), new FT(), new LMT(), new RandomTree(), new SimpleCart(),
        new IBk(),
        new ConjunctiveRule(), new JRip(), new Ridor(),
        new DecisionTable(),
        new VFI()};

    // Called for 60-40 Split:: initialise the class: Keep Using
    // Instances trainData, 
    public EvaluateMOEnsembleObjs(String ensmCmb, String strFP, boolean mc) {
        //trainFullData = trainData;
        strCmb = ensmCmb;
        strFsPct = strFP;
        isMultiClass = mc;
        if (mc == true) {
            baseClassifiers = baseClassifiersMultiClass;
        } else {
            baseClassifiers = baseClassifiersBinaryClass;
        }
    }

    // Instances trainData,
    public EvaluateMOEnsembleObjs(String ensmCmb, String strFP, boolean mc, boolean isPar) {
        //trainFullData = trainData;
        strCmb = ensmCmb;
        strFsPct = strFP;
        isMultiClass = mc;
        if (mc == true) {
            baseClassifiers = baseClassifiersMultiClass;
        } else {
            baseClassifiers = baseClassifiersBinaryClass;
        }
        isParallel = isPar;
    }

    // Called for k-fold CV :: calculate MCC : Keep Using
    public double getCVFitnessMCC(Instances trainFullData, int kFold) {
        if (isCVEvalDone == false) {
            Evaluation eval;
            try {
                // randomize data

                Random rand = new Random(1);
                Instances randData = new Instances(trainFullData);
                randData.randomize(rand);
                if (randData.classAttribute().isNominal()) {
                    randData.stratify(kFold);
                }

                // evaluate EOC and get mcc
                eval = new Evaluation(trainFullData);
                double cvDivr = 0.0;
                // perform cross-validation
                for (int n = 0; n < kFold; n++) {
                    Instances train = randData.trainCV(kFold, n);
                    Instances test = randData.testCV(kFold, n);

                    // Save for using from calcDiversity
                    //spltTest = test;
                    mSampTest = test.numInstances();

                    //EnsembleOfClassifierMajVote myEOC = null;
                    EnsembleOfClassifierMajVote myCVEOC;

                    if (isParallel == true) {
                        myCVEOC = buildEnsembleModelParallel(train, test);
                    } else {
                        myCVEOC = buildEnsembleModelSerial(train, test);
                    }

                    eval.evaluateModel(myCVEOC, test);

                    double divScore = calcDiversityScore(test);
                    //isCalcDiv = false; // preventing using split div score

                    cvDivr += divScore;
                }
                cvMCC = eval.weightedMatthewsCorrelation();
                cvDivrScore = cvDivr / kFold;
                isCVEvalDone = true;
            } catch (Exception ex) {
                Logger.getLogger(EvaluateMOEnsembleObjs.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return cvMCC;
    }

    // Called for k-fold CV :: calculate Diversity Score : Keep Using
    public double getCVDiversityScore() {
        return (cvDivrScore);
    }

// Called for 60-40 Split:: Calculate MCC: Keep Using
    public double getSplitFitnessMCC(Instances trainFullData) {
        if (isSplitEvalDone == false) {
            try {
                // 60-40 stratified random split of training data
                Instances randData = new Instances(trainFullData);   // create copy of original data
                randData.randomize(new java.util.Random(1));         // randomize data with number generator

                int trainSize = (int) Math.round(randData.numInstances() * 0.6);
                int testSize = randData.numInstances() - trainSize;
                Instances train = new Instances(randData, 0, trainSize);
                Instances test = new Instances(randData, trainSize, testSize);

// Save test Split for diversity calculation
                //spltTest = test;
                mSampTest = test.numInstances();

// build models using train split  
                EnsembleOfClassifierMajVote myEOC;

                if (isParallel == true) {
                    myEOC = buildEnsembleModelParallel(train, test);
                } else {
                    myEOC = buildEnsembleModelSerial(train, test);
                }
                //myEOC = buildEnsembleModelSerial(train, test);
                //myEOC = buildEnsembleModelParallel(train, test);

// evaluate EOC and get the mcc
                Evaluation eval = new Evaluation(train);
                eval.evaluateModel(myEOC, test);

                splitMCC = eval.weightedMatthewsCorrelation();
                splitDivrScore = calcDiversityScore(test);

                isSplitEvalDone = true;
            } catch (Exception ex) {
                if (globalDebug == true) {
                    System.err.println("Error evaluating MCC: " + strCmb);
                }
            }
        }
        return splitMCC;
    }

// Called for 60-40 Split:: get diversity : keep Using
    public double getSplitDiversityScore() {
        return splitDivrScore;
    }

// Called for 60-40 Split:: diversity calculation : Keep Using
    double calcDiversityScore(Instances spltTest) {
        int si = 0;
        double divScore = 0.0;
        int N = mSampTest; //number of samples
        int L = ensmSize; //number of classifiers

        /*
         if (isModelBuild == false) //Build models in parallel if yet to build
         {
         buildEnsembleModelSerial();
         }*/

        /*
         @article{kuncheva2003measures,
         title={Measures of diversity in classifier ensembles and their relationship with the ensemble accuracy},
         author={Kuncheva, Ludmila I and Whitaker, Christopher J},
         journal={Machine learning},
         volume={51},
         number={2},
         pages={181--207},
         year={2003},
         publisher={Springer}
         }
         Page: 185
         Section: 4.1 The Entropy Measure E
         Equation (8)
         E = 1/N * sum_j=1:N{ 1/(L-ceil(L/2)) * {min(#correct,  L-#correct)}
         N= mSamples, L=ensmSize
        
         Matlab Code http://pages.bangor.ac.uk/~mas00a/book_wiley/matlab_code/diversity/demo_diversity.html
         */
        double divj = 0.0;
        try {
            double mult = 1.0 / (L - Math.ceil(L / 2.0));

            for (int j = 0; j < N; j++) {
                int lzj = 0;    // l(z_j)

                for (int ci = 0; ci < L; ci++) {
                    double pred = ensmPredictions[ci][si];
                    double orig = spltTest.instance(si).classValue();
                    if (orig == pred) {
                        lzj++;
                    }
                }

                int min = 0;
                int nlzj = (L - lzj); // L-l(z_j)
                if (lzj <= nlzj) {
                    min = lzj;
                } else {
                    min = nlzj;
                }
                divj += (mult * min);
            }
        } catch (Exception e) {
            System.err.println("Exception for : " + strFsPct + strCmb + " Smaple:" + si);
            e.printStackTrace();
        }
        divScore = (divj / N);
        return divScore;
    }

// Called for 60-40 Split & k-fold CV :: build models : Keep Using
    private EnsembleOfClassifierMajVote buildEnsembleModelSerial(Instances train, Instances test) {

        String clsName = "";
        //int numOfTasks = numOfClassifiers(strCmb);
        //ensmPredictions = new double[numOfTasks][mSampTest];
        List<Classifier> buildClsf = new ArrayList<Classifier>();

        //System.out.print("Evaluate: ");
        for (int i = 0; i < strCmb.length(); i++) {

            if (strCmb.charAt(i) == '1') {
                try {
                    // build and store the model into ensemble
                    Classifier clsCopy = AbstractClassifier.makeCopy(baseClassifiers[i]);
                    clsName = clsCopy.getClass().getSimpleName();
                    clsCopy.buildClassifier(train);
                    buildClsf.add(clsCopy);
                } catch (Exception ex) {
                    System.err.println("Unable to build model... " + clsName + " for: " + strFsPct + strCmb);
                }
            }
        }

        //set build models & predictions       
        ensmSize = buildClsf.size();
        ensmPredictions = new double[ensmSize][mSampTest];
        EnsembleOfClassifierMajVote buildEOC = new EnsembleOfClassifierMajVote();

        int cidx = 0;   // classifier index
        for (Classifier clsBuildCopy : buildClsf) {
            try {
                // Save model
                buildEOC.addPreBuiltClassifier(clsBuildCopy);

                // get prediction vector
                double predictions[] = new double[mSampTest];
                try {
                    Evaluation eval = new Evaluation(train);
                    predictions = eval.evaluateModel(clsBuildCopy, test);
                } catch (Exception ex) {
                    //Logger.getLogger(EvaluateMOEnsembleObjs.class.getName()).log(Level.SEVERE, null, ex);
                    if (globalDebug == true) {
                        System.err.println("Error Geting Predictions: " + strCmb);
                    }
                }
                ensmPredictions[cidx] = predictions;
                cidx++;
            } catch (Exception e) {
                System.err.println("Unable to get build model... " + clsBuildCopy.getClass().getSimpleName() + " for: " + strFsPct + strCmb);
            }
        }
        //isModelBuild = true; // model building succesful
        return buildEOC;
    }

    private int jobCounter(String cmb) {
        int tot = 0;
        for (int i = 0; i < cmb.length(); i++) {
            if (cmb.charAt(i) == '1') {
                tot++;
            }
        }
        return tot;
    }

    private EnsembleOfClassifierMajVote buildEnsembleModelParallel(Instances train, Instances test) {
        String clsName = "";
        int numOfTasks = jobCounter(strCmb);
        //ensmPredictions = new double[numOfTasks][mSampTest];
        ForkJoinPool fjPool = new ForkJoinPool(numOfTasks);
        ArrayList<Future> futuresList = new ArrayList<Future>();

        //Start accumulating models
        for (int i = 0; i < strCmb.length(); i++) {
            if (strCmb.charAt(i) == '1') {
                try {
                    // build and store the model into ensemble
                    Classifier clsCopy = AbstractClassifier.makeCopy(baseClassifiers[i]);
                    futuresList.add(fjPool.submit(new FJModelBuilder(clsCopy, train)));
                } catch (Exception ex) {
                    System.err.println("Unable to copy base classifier: " + baseClassifiers[i].getClass().getSimpleName());
                }
            }
        }

        // Start building models
        List<Classifier> buildClsf = new ArrayList<Classifier>();
        Classifier builtModel;
        for (Future future : futuresList) {
            try {
                builtModel = AbstractClassifier.makeCopy((Classifier) future.get());
                buildClsf.add(builtModel);
            } catch (Exception ex) {
                Logger.getLogger(EvaluateMOEnsembleObjs.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //set build models & predictions       
        ensmSize = buildClsf.size();
        ensmPredictions = new double[ensmSize][mSampTest];
        EnsembleOfClassifierMajVote buildEOC = new EnsembleOfClassifierMajVote();

        int cidx = 0;   // classifier index
        for (Classifier clsBuildCopy : buildClsf) {
            try {
                // Save model
                buildEOC.addPreBuiltClassifier(clsBuildCopy);

                // get prediction vector
                double predictions[] = new double[mSampTest];
                try {
                    Evaluation eval = new Evaluation(train);
                    predictions = eval.evaluateModel(clsBuildCopy, test);
                } catch (Exception ex) {
                    //Logger.getLogger(EvaluateMOEnsembleObjs.class.getName()).log(Level.SEVERE, null, ex);
                    if (globalDebug == true) {
                        System.err.println("Error Geting Predictions: " + strCmb);
                    }
                }
                ensmPredictions[cidx] = predictions;
                cidx++;
            } catch (Exception e) {
                System.err.println("Unable to get build model... " + clsBuildCopy.getClass().getSimpleName() + " for: " + strFsPct + strCmb);
            }
        }
        //isModelBuild = true; // model building succesful
        return buildEOC;
    }

    // evaluate ensemble combination
    // Instances trainData, Instances testData,
    public EvaluateMOEnsembleObjs(String ensmCmb, boolean mc) {
        //evalTrainData = trainData;
        //evalTestData = testData;
        strCmb = ensmCmb;
        isMultiClass = mc;
        if (mc == true) {
            baseClassifiers = baseClassifiersMultiClass;
        } else {
            baseClassifiers = baseClassifiersBinaryClass;
        }
    }

    // evaluate ensemble combination
    // Instances trainData, 
    /*public EvaluateMOEnsembleObjs(String ensmCmb, boolean mc) {
     //evalTrainData = trainData;
     strCmb = ensmCmb;
     isMultiClass = mc;
     if (mc == true) {
     baseClassifiers = baseClassifiersMultiClass;
     } else {
     baseClassifiers = baseClassifiersBinaryClass;
     }
     }*/
    public Evaluation evaluateCVCombinationEoC(Instances evalTrainData) throws Exception {
        // randomize data
        int kFold = 10;
        Random rand = new Random(1);
        Instances randData = new Instances(evalTrainData);
        randData.randomize(rand);
        if (randData.classAttribute().isNominal()) {
            randData.stratify(kFold);
        }

        // evaluate EOC and get mcc
        Evaluation eval = new Evaluation(evalTrainData);
        // perform cross-validation
        for (int n = 0; n < kFold; n++) {
            Instances train = randData.trainCV(kFold, n);
            Instances test = randData.testCV(kFold, n);

            EnsembleOfClassifierMajVote myEOC = buildEnsembleModelSerial(train);
            eval.evaluateModel(myEOC, test);
        }
        return eval;
    }

    public Evaluation evaluateCombinationEoC(Instances evalTrainData, Instances evalTestData) throws Exception {
        Evaluation eval = null;

        // build models using train split   
        EnsembleOfClassifierMajVote buildEOC = null;
//System.err.println("Init: " + buildEOC.toString());

        buildEOC = buildEnsembleModelSerial(evalTrainData);
        //System.err.println("Built: " + buildEOC.toString());
        try {
            InputMappedClassifier inpMappedEoC = new InputMappedClassifier();
            String mapperArgs[] = {"-I", "-trim", "-M"};

            inpMappedEoC.setOptions(mapperArgs);
            inpMappedEoC.setClassifier(buildEOC);
            inpMappedEoC.buildClassifier(evalTrainData);

            eval = new Evaluation(evalTrainData);
            eval.evaluateModel(inpMappedEoC, evalTestData);
        } catch (Exception ex) {
            Logger.getLogger(EvaluateMOEnsembleObjs.class.getName()).log(Level.SEVERE, null, ex);
        }
        return eval;
    }

    private EnsembleOfClassifierMajVote buildEnsembleModelSerial(Instances train) {
        String clsName = "";
        EnsembleOfClassifierMajVote buildEOC = new EnsembleOfClassifierMajVote();
        buildEOC.removeUntrainedZeroR();

        for (int i = 0; i < strCmb.length(); i++) {
            if (strCmb.charAt(i) == '1') {
                try {
                    // build and store the model into ensemble
                    Classifier clsCopy = AbstractClassifier.makeCopy(baseClassifiers[i]);
                    clsName = clsCopy.getClass().getSimpleName();
                    clsCopy.buildClassifier(train);
                    buildEOC.addPreBuiltClassifier(clsCopy);
                } catch (Exception ex) {
                    System.err.println("Unable to build model... " + clsName + " for: " + strCmb);
                }
            }
        }
        return buildEOC;
    }
}

/*
    
 private EnsembleOfClassifierMajVote buildEnsembleModelSerialTst(Instances train) {

 String clsName = "";
 EnsembleOfClassifierMajVote buildEOC = new EnsembleOfClassifierMajVote();
 buildEOC.removeUntrainedZeroR();

 for (int i = 0; i < strCmb.length(); i++) {
 if (strCmb.charAt(i) == '1') {
 try {
 // build and store the model into ensemble
 Classifier clsCopy = AbstractClassifier.makeCopy(baseClassifiers[i]);
 clsName = clsCopy.getClass().getSimpleName();
 clsCopy.buildClassifier(train);
 buildEOC.addPreBuiltClassifier(clsCopy);
 } catch (Exception ex) {
 System.err.println("Unable to build model... " + clsName + " for: " + strCmb);
 }
 }
 }

 // Set Classifier Input Mapper
 InputMappedClassifier inpMapper = null;
 try {
 inpMapper = new InputMappedClassifier();
 String mapperArgs[] = {"-I", "-trim", "-M"};

 inpMapper.setOptions(mapperArgs);
 inpMapper.setClassifier(buildEOC);
 inpMapper.buildClassifier(train);
 } catch (Exception ex) {
 Logger.getLogger(EvaluateMOEnsembleObjs.class.getName()).log(Level.SEVERE, null, ex);
 }
 return inpMapper;
       
 }
 }

 public void crossValidEvalEoC() throws Exception {
 int folds = 10;
 // randomize data
 // evaluate EOC and get mcc
 eval = new Evaluation(trainFullData);

 Random rand = new Random(1);
 Instances randData = new Instances(trainFullData);
 randData.randomize(rand);
 if (randData.classAttribute().isNominal()) {
 randData.stratify(folds);
 }

 // perform cross-validation
 for (int n = 0; n < folds; n++) {
 train = randData.trainCV(folds, n);
 test = randData.testCV(folds, n);

 EnsembleOfClassifierMajVote myEOC = eBuildEnsembleModelSerial();
 eval.evaluateModel(myEOC, test);
 }
 }

 public void evaluateEoC(Instances train, Instances test) throws Exception {

 EnsembleOfClassifierMajVote myEOC = buildTestEnsembleModelSerial(train);
 System.out.println(myEOC.toString());
 // evaluate EOC and get mcc
 eval = new Evaluation(train);

 // serialize model
 weka.core.SerializationHelper.write(strCmb + ".model", myEOC);
 eval.evaluateModel(myEOC, test);
 }

 private EnsembleOfClassifierMajVote eBuildEnsembleModelSerial() {
 String clsName = "";
 //int numOfTasks = numOfClassifiers(strCmb);
 //ensmPredictions = new double[numOfTasks][mSampTest];
 List<Classifier> buildClsf = new ArrayList<Classifier>();

 for (int i = 0; i < strCmb.length(); i++) {
 if (strCmb.charAt(i) == '1') {
 try {
 // build and store the model into ensemble
 Classifier clsCopy = AbstractClassifier.makeCopy(baseClassifiers[i]);
 clsName = clsCopy.getClass().getSimpleName();
 clsCopy.buildClassifier(train);
 buildClsf.add(clsCopy);
 } catch (Exception ex) {
 System.err.println("Unable to build model... " + clsName + " for: " + strFsPct + strCmb);
 }
 }
 }

 EnsembleOfClassifierMajVote buildEOC = new EnsembleOfClassifierMajVote();
 for (Classifier clsBuildCopy : buildClsf) {
 try {
 // Save model
 buildEOC.addPreBuiltClassifier(clsBuildCopy);
 } catch (Exception e) {
 System.err.println("Unable to get build model... " + clsBuildCopy.getClass().getSimpleName() + " for: " + strFsPct + strCmb);
 }
 }
 isModelBuild = true; // model building succesful
 return buildEOC;
 }

 private EnsembleOfClassifierMajVote buildTestEnsembleModelSerial(Instances trainData) throws Exception {
 String clsName = "";
 List<Classifier> buildClsf = new ArrayList<Classifier>();

 for (int i = 0; i < strCmb.length(); i++) {
 if (strCmb.charAt(i) == '1') {
 try {
 // build and store the model into ensemble
 Classifier clsCopy = AbstractClassifier.makeCopy(baseClassifiers[i]);
 clsName = clsCopy.getClass().getSimpleName();
 clsCopy.buildClassifier(trainData);
 buildClsf.add(clsCopy);
 } catch (Exception ex) {
 System.err.println("Unable to build model... " + clsName + " for: " + strFsPct + strCmb);
 }
 }
 }

 //set build models & predictions       
 ensmSize = buildClsf.size();
 EnsembleOfClassifierMajVote myEOC = new EnsembleOfClassifierMajVote();

 for (Classifier clsBuildCopy : buildClsf) {
 try {
 // Save model
 myEOC.addPreBuiltClassifier(clsBuildCopy);
 } catch (Exception e) {
 System.err.println("Unable to get build model... " + clsBuildCopy.getClass().getSimpleName() + " for: " + strFsPct + strCmb);
 }
 }
 isModelBuild = true; // model building succesful
 return myEOC;
 }

 private double[] getPredictions(Classifier clsBuildCopy) {
 double predictions[] = new double[mSampTest];
 try {
 Evaluation eval = new Evaluation(train);
 predictions = eval.evaluateModel(clsBuildCopy, test);
 } catch (Exception ex) {
 //Logger.getLogger(EvaluateMOEnsembleObjs.class.getName()).log(Level.SEVERE, null, ex);
 if (globalDebug == true) {
 System.err.println("Error Geting Predictions: " + strCmb);
 }
 }
 return predictions;
 }

 public Evaluation getEvaluation() {
 return eval;
 }

 public Evaluation getTestEvaluation() {
 return testEval;
 }

 public int getSize() {
 return ensmSize;
 }

 */

/*
 public Classifier getTrainedEnsembleMajVote(EnsembleOfClassifierMajVote myEOC) {
 Classifier eocMajVote = (Classifier) myEOC;
 return eocMajVote;
 }*/
