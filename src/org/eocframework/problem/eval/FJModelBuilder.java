/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eocframework.problem.eval;

import java.util.Random;
import java.util.concurrent.RecursiveTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.eocframework.problem.eval.EvaluateMOEnsembleObjs.baseClassifiers;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

/**
 *
 * @author mohammad
 */
public class FJModelBuilder extends RecursiveTask {

    private static final long serialVersionUID = 1125899839733759L;  // one of Carol prime number

    private Instances trnData;
    private Classifier cls;

    public FJModelBuilder(Classifier cls, Instances trn) {
        this.trnData = trn;
        this.cls = cls;
    }

    public Classifier compute() {
        Classifier clsCopy = null;
        try {
            // build and store the model into ensemble
            clsCopy = AbstractClassifier.makeCopy(cls);
            clsCopy.buildClassifier(trnData);

        } catch (Exception ex) {
            Logger.getLogger(FJModelBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clsCopy;
    }
}
