/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eocframework.problem.eval;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import weka.classifiers.Classifier;
import weka.classifiers.MultipleClassifiersCombiner;
import weka.core.Aggregateable;
import weka.core.Capabilities;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.OptionHandler;
import weka.core.Utils;

public class EnsembleOfClassifierMajVote extends MultipleClassifiersCombiner implements
        Aggregateable<Classifier> {

    /**
     * for serialization
     */
    static final long serialVersionUID = 1125899839733759L;
    protected Random m_Random = new Random(1);
    /**
     * List of de-serialized pre-built classifiers to include in the ensemble
     */
    protected List<Classifier> m_preBuiltClassifiers = new ArrayList<Classifier>();
    protected List<Classifier> m_baseClassifiers = new ArrayList<Classifier>();

    /**
     * Structure of the training data
     */
    protected Instances m_structure;

    public EnsembleOfClassifierMajVote() {
        setClassifiers(new Classifier[0]);
    }

    /**
     * Returns default capabilities of the classifier.
     *
     * @return the capabilities of this classifier
     */
    @Override
    public Capabilities getCapabilities() {
        Capabilities result = super.getCapabilities();

        if (m_preBuiltClassifiers.size() > 0) {
            if (m_Classifiers.length == 0) {
                result = (Capabilities) m_preBuiltClassifiers.get(0).getCapabilities()
                        .clone();
            }
            for (int i = 1; i < m_preBuiltClassifiers.size(); i++) {
                result.and(m_preBuiltClassifiers.get(i).getCapabilities());
            }

            for (Capabilities.Capability cap : Capabilities.Capability.values()) {
                result.enableDependency(cap);
            }
        }

        // class for MAJORITY_VOTING_RULE
        result.disableAllClasses();
        result.disableAllClassDependencies();
        result.enable(Capabilities.Capability.NOMINAL_CLASS);
        result.enableDependency(Capabilities.Capability.NOMINAL_CLASS);

        return result;
    }

    /**
     * Buildclassifier selects a classifier from the set of classifiers by
     * minimising error on the training data.
     *
     * @param data the training data to be used for generating the boosted
     * classifier.
     * @throws Exception if the classifier could not be built successfully
     */
    @Override
    public void buildClassifier(Instances data) throws Exception {
        // can classifier handle the data?
        //getCapabilities().testWithFail(data);
        if (m_baseClassifiers.size() > 0) {
            for (Classifier clsf : m_baseClassifiers) {
                clsf.buildClassifier(data);;
                addPreBuiltClassifier(clsf);
            }
        }

    }

    /**
     * Add a prebuilt classifier to the list for use in the ensemble
     *
     * @param c a prebuilt Classifier to add.
     */
    public void addPreBuiltClassifier(Classifier c) {
        m_preBuiltClassifiers.add(c);
    }

    public void addClassifier(Classifier c) {
        m_baseClassifiers.add(c);
    }
    /*
     * Sets the list of possible classifiers to choose from.
     */

    public void setPreBuiltClassifiers(List<Classifier> arr_classifiers) {
        m_preBuiltClassifiers = arr_classifiers;
    }

    public void setClassifiers(List<Classifier> arr_classifiers) {
        m_baseClassifiers = arr_classifiers;
    }

    /**
     * Remove a prebuilt classifier from the list to use in the ensemble
     *
     * @param c the classifier to remove
     */
    public void removePreBuiltClassifier(Classifier c) {
        m_preBuiltClassifiers.remove(c);
    }

    public void removeClassifier(Classifier c) {
        m_preBuiltClassifiers.remove(c);
    }

    public void removeUntrainedZeroR() {
        // remove the single untrained ZeroR
        setClassifiers(new Classifier[0]);
    }

    /**
     * Classifies the given test instance.
     *
     * @param instance the instance to be classified
     * @return the predicted most likely class for the instance or
     * Utils.missingValue() if no prediction is made
     * @throws Exception if an error occurred during the prediction
     */
    @Override
    public double classifyInstance(Instance instance) throws Exception {
        double result;
        double[] dist;
        int index;

        dist = distributionForInstance(instance);
        if (instance.classAttribute().isNominal()) {
            index = Utils.maxIndex(dist);
            if (dist[index] == 0) {
                result = Utils.missingValue();
            } else {
                result = index;
            }
        } else if (instance.classAttribute().isNumeric()) {
            result = dist[0];
        } else {
            result = Utils.missingValue();
        }
        return result;
    }

    /**
     * Classifies a given instance using the selected combination rule.
     *
     * @param instance the instance to be classified
     * @return the distribution
     * @throws Exception if instance could not be classified successfully
     */
    @Override
    public double[] distributionForInstance(Instance instance) throws Exception {
        double[] result = new double[instance.numClasses()];
        result = distributionForInstanceMajorityVoting(instance);
        if (!instance.classAttribute().isNumeric() && (Utils.sum(result) > 0)) {
            Utils.normalize(result);
        }
        return result;
    }

    /**
     * Classifies a given instance using the Majority Voting combination rule.
     *
     * @param instance the instance to be classified
     * @return the distribution
     * @throws Exception if instance could not be classified successfully
     */
    protected double[] distributionForInstanceMajorityVoting(Instance instance)
            throws Exception {

        double[] probs = new double[instance.classAttribute().numValues()];
        double[] votes = new double[probs.length];

        for (int i = 0; i < m_Classifiers.length; i++) {
            probs = getClassifier(i).distributionForInstance(instance);
            int maxIndex = 0;
            for (int j = 0; j < probs.length; j++) {
                if (probs[j] > probs[maxIndex]) {
                    maxIndex = j;
                }
            }

            // Consider the cases when multiple classes happen to have the same
            // probability
            for (int j = 0; j < probs.length; j++) {
                if (probs[j] == probs[maxIndex]) {
                    votes[j]++;
                }
            }
        }

        for (int i = 0; i < m_preBuiltClassifiers.size(); i++) {
            probs = m_preBuiltClassifiers.get(i).distributionForInstance(instance);
            int maxIndex = 0;

            for (int j = 0; j < probs.length; j++) {
                if (probs[j] > probs[maxIndex]) {
                    maxIndex = j;
                }
            }

            // Consider the cases when multiple classes happen to have the same
            // probability
            for (int j = 0; j < probs.length; j++) {
                if (probs[j] == probs[maxIndex]) {
                    votes[j]++;
                }
            }
        }

        int tmpMajorityIndex = 0;
        for (int k = 1; k < votes.length; k++) {
            if (votes[k] > votes[tmpMajorityIndex]) {
                tmpMajorityIndex = k;
            }
        }

        // Consider the cases when multiple classes receive the same amount of votes
        List<Integer> majorityIndexes = new ArrayList<Integer>();
        for (int k = 0; k < votes.length; k++) {
            if (votes[k] == votes[tmpMajorityIndex]) {
                majorityIndexes.add(k);
            }
        }

        int majorityIndex = 0;
        int noOfTies = majorityIndexes.size();
        // Resolve the ties according to a uniform random distribution
        if (noOfTies > 1) {
            int randPos = m_Random.nextInt(noOfTies - 1);
            majorityIndex = majorityIndexes.get(randPos);
        } else {
            majorityIndex = tmpMajorityIndex;
        }

        // set probs to 0
        probs = new double[probs.length];

        probs[majorityIndex] = 1; // the class that have been voted the most
        // receives 1

        return probs;
    }

    /**
     * Output a representation of this classifier
     *
     * @return a string representation of the classifier
     */
    @Override
    public String toString() {

        if (m_Classifiers == null) {
            return "Ensemble of Classifier Majority Vote: No model built yet.";
        }

        String result = "Ensemble of Classifier Majority Vote combines";
        result += " the probability distributions of these base learners:\n";
        for (int i = 0; i < m_Classifiers.length; i++) {
            result += '\t' + getClassifierSpec(i) + '\n';
        }

        if (m_baseClassifiers.size() == 0) {
            for (Classifier c : m_preBuiltClassifiers) {
                result += "\t" + c.getClass().getSimpleName()
                        + Utils.joinOptions(((OptionHandler) c).getOptions()) + "\n";
            }
        } else {
            for (Classifier c : m_baseClassifiers) {
                result += "\t" + c.getClass().getSimpleName()
                        + Utils.joinOptions(((OptionHandler) c).getOptions()) + "\n";
            }
        }

        result += "using the '";
        result += "Majority Voting";
        result += "' combination rule \n";

        return result;
    }

    /**
     * Aggregate an object with this one
     *
     * @param toAggregate the object to aggregate
     * @return the result of aggregation
     * @throws Exception if the supplied object can't be aggregated for some
     * reason
     */
    @Override
    public Classifier aggregate(Classifier toAggregate) throws Exception {

        if (m_structure == null && m_Classifiers.length == 1
                && (m_Classifiers[0] instanceof weka.classifiers.rules.ZeroR)) {
            // remove the single untrained ZeroR
            setClassifiers(new Classifier[0]);
        }

        // Can't do any training data compatibility checks unfortunately
        addPreBuiltClassifier(toAggregate);

        return this;
    }

    /**
     * Call to complete the aggregation process. Allows implementers to do any
     * final processing based on how many objects were aggregated.
     *
     * @throws Exception if the aggregation can't be finalized for some reason
     */
    @Override
    public void finalizeAggregation() throws Exception {
        // nothing to do
    }

    /**
     * Main method for testing this class.
     *
     * @param argv should contain the following arguments: -t training file [-T
     * test file] [-c class index]
     */
    public static void main(String[] argv) {
        runClassifier(new EnsembleOfClassifierMajVote(), argv);
    }

}
